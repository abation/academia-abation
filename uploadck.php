<?php
	
	/*$sourcePath = $_FILES["file"]['tmp_name'];
	//$destination_img = 'aprobar' . round(microtime(true) * 1000) . '.jpg';
	$targetPath = "uploads/".$_FILES['file']['name'];

	if ($_FILES["file"]["size"] > 2000000) {
    echo '0'; //Error: demasiado pesado
  }
  else { 
  	if ( 0 < $_FILES['file']['error'] ) {
      //echo 'Error: ' . $_FILES['file']['error'] . '<br>';
      echo '-1';//Error: dentro del file, a saber
    }
    else {

    	//$d = compress($source_img, $destination_img, 75);
      if (move_uploaded_file($sourcePath,$targetPath) {
      	echo '[img]http://beta.abation.es/academia/uploads/'. $destination_img .'[/img]';
      }
      else {
      	echo '-2';//Error (es posible que por los permisos de la carpeta en el ftp, ver si uploads/ tiene 777)
      }
    }
	}*/

	/*function compress($source, $destination, $quality) { 
		$info = getimagesize($source); 

		if ($info['mime'] == 'image/jpeg') 
			$image = imagecreatefromjpeg($source); 
		elseif ($info['mime'] == 'image/gif') 
			$image = imagecreatefromgif($source); 
		elseif ($info['mime'] == 'image/png') 
			$image = imagecreatefrompng($source); 

		imagejpeg($image, $destination, $quality); 
		return $destination; 
	}*/

	if(isset($_FILES["file"]["type"])) {
		$validextensions = array("jpeg", "jpg", "png", "gif");
		$temporary = explode(".", $_FILES["file"]["name"]);
		$file_extension = end($temporary);
		
		if ((($_FILES["file"]["type"] == "image/png") || ($_FILES["file"]["type"] == "image/jpg") || 
			($_FILES["file"]["type"] == "image/jpeg") || ($_FILES["file"]["type"] == "image/gif")) && 
			($_FILES["file"]["size"] < 2000000) && in_array($file_extension, $validextensions))  {
		
			if ($_FILES["file"]["error"] > 0) {
				//echo "Return Code: " . $_FILES["file"]["error"] . "<br/><br/>";
				echo '-1';
			}
			else
			{
				/*if (file_exists("uploads/" . $_FILES["file"]["name"])) {
					echo '-2';//File already exists
				}
				else {*/
					$sourcePath = $_FILES['file']['tmp_name']; // Storing source path of the file in a variable
					$targetPath = "uploads/aprueba".round(microtime(true) * 1000).$_FILES['file']['name']; // Target path where file is to be stored
					$server_url = "http://$_SERVER[HTTP_HOST]/";
					
					if(move_uploaded_file($sourcePath,$targetPath)) {
						$fullPath = '[img]'. $server_url . $targetPath .'[/img]';
						setDeviceRotation($targetPath);
						echo $fullPath;
					}
					else {
						echo '-3';//Cannot move
					}

					/*echo "<span id='success'>Image Uploaded Successfully...!!</span><br/>";
					echo "<br/><b>File Name:</b> " . $_FILES["file"]["name"] . "<br>";
					echo "<b>Type:</b> " . $_FILES["file"]["type"] . "<br>";
					echo "<b>Size:</b> " . ($_FILES["file"]["size"] / 1024) . " kB<br>";
					echo "<b>Temp file:</b> " . $_FILES["file"]["tmp_name"] . "<br>";*/
				//}
			}
		}
		else {
			//echo "<span id='invalid'>***Invalid file Size or Type***<span>";
			echo '0';
		}
	}

	// Input: $filePath : path to image file
	// Output: If there is any orientation required then function will change that image accordingly
	function setDeviceRotation($filePath) {
		$exif = exif_read_data($filePath);
		 
		//if there is orientation change
		$exif_orient = isset($exif['Orientation']) ? $exif['Orientation'] : 0;
		$rotateImage = 0;
		 
		//convert exif rotation to angles
		if (6 == $exif_orient) {
			$rotateImage = 90;
			$imageOrientation = 1;
		}
		elseif (3 == $exif_orient) {
			$rotateImage = 180;
			$imageOrientation = 1;
		}
		elseif (8 == $exif_orient) {
			$rotateImage = 270;
			$imageOrientation = 1;
		}

		//if the image is rotated
		if ($rotateImage) {
			$rotateImage = -$rotateImage;
			$info   = getimagesize($filePath);
			$fileType = $info[2];
			
			switch ($fileType) {
				case '1':
					$source = imagecreatefromgif($filePath);
					$rotate = imagerotate($source, $rotateImage, 0);
					imagegif($rotate,$filePath);
					break;

				case '2': //image/jpeg
					$source = imagecreatefromjpeg($filePath);
					$rotate = imagerotate($source, $rotateImage, 0);
					imagejpeg($rotate,$filePath);
					break;
				
				case '3': //image/png
					$source = imagecreatefrompng($filePath);
					$rotate = imagerotate($source, $rotateImage, 0);
					imagepng($rotate,$filePath);
					break;
				
				default:
					break;
			}
		}

		// The image orientation is fixed, pass it back for further processing
		return $file;
	}
