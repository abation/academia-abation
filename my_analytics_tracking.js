$(document).ready(function() {
	//a tags que van a tutoriales
	$('a[href="http://tutoriales.abation.es"]').each(function(index, element) {
		if ( $(this).childElementCount < 1 ) {
			$(this).on('click', function() {
				ga('send', 'event', 'button', 'click', 'tutoriales-menu-superior');
			});
		}
		else { 
			$(this).on('click', function() {
				ga('send', 'event', 'button', 'click', 'tutoriales-menu-izquierda');
			});
		}
	});

	//button top left "Haz tu pregunta"
	$('.ask-question').on('click', function() {
		ga('send', 'event', 'button', 'click', 'haz-tu-pregunta');
	});

	//buttons login/register
	$('#login-link').on('click', function() {
		ga('send', 'event', 'button', 'click', 'login');
	});

	$('#register-link').on('click', function() {
		ga('send', 'event', 'button', 'click', 'register');
	});

	//left menu Categorías link click
	$('a[href="http://academia.abation.es/categories/"]').each(function(index, element) {
		$(this).on('click', function() {
			ga('send', 'event', 'button', 'click', 'categorias-menu-izquierda');
		});
	});

	//Home or inside a question -> click on specific category
	$('.question-category').children().each(function(index, element) {
		var label = $(this).text();
		label = label.toString().substring(0, label.length - 1);
		label = label.toString().toLowerCase();

		$(this).click({param1: label}, sendDataToAnalytics);

		/*$(this).on('click', function(label) {
			ga('send', 'event', 'button', 'click', 'category-' + label);
		});*/
	});

	//Category page -> click on specific category
	$('.cat-item').children().each(function(index, element) {
		var label = $(this).text();
		label = label.toString().toLowerCase();

		$(this).click({param1: label}, sendDataToAnalytics);

		/*$(this).on('click', function(label) {
			ga('send', 'event', 'button', 'click', 'category' + label);
		});*/
	});
});

function sendDataToAnalytics(event) {
	ga('send', 'event', 'button', 'click', 'category-' + event.data.param1);
}

