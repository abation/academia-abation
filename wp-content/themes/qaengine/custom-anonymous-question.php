<?php
/**
 * Template Name: Custom Anonymous Question Template
 * version 1.0
 * @author: Steven Máñez - Heredero al trono de Isengard (Abation)
 **/
?>

<?php
	if (is_user_logged_in()) {
	    wp_redirect( home_url() ); 
	    exit;
	} 
?>

<?php session_start() ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="utf-8">
        <?php
			$options = AE_Options::get_instance();
			$favicon = $options->mobile_icon['thumbnail'][0];
        ?>
        <title><?php wp_title(); ?></title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="shortcut icon" href="<?php echo $favicon ?>"/>
		<link href='//fonts.googleapis.com/css?family=Lato:400,700&subset=latin,cyrillic,cyrillic-ext,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/libs/selectivizr-min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
		
		<?php
		    //loads comment reply JS on single posts and pages
		    if ( is_single()) wp_enqueue_script( 'comment-reply' );
	    ?>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	    <?php wp_head(); ?>

	    <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		  ga('create', 'UA-61340191-2', 'auto');
		  ga('send', 'pageview');
		</script>

	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/myFormScripts/custom-anonymous-question.js"></script>

	    <script>
	    	var flag = false;

	    	$(document).ready(function() {	
	    		/*$("#question_title").click(function(){
	    			showModal();
	    		});

	    		$("#question_content").click(function(){
	    			showModal();
	    		});*/

	    		/*$("#question_category").click(function(){
	    			if (!flag) {
	    				$('#myModal').modal('show');
	    				flag = true;
	    			}
	    		});*/
	    		setTimeout(showModal, 3000);

	    		$('#custom-login-button').hover(
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light');
				    	$(this).addClass('mint-button-secondary--light-inverse'); 
				    },
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light-inverse'); 
				    	$(this).addClass('mint-button-secondary--light'); 
				    }
				)

				$('#custom-register-button').hover(
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light-inverse'); 
				    	$(this).addClass('mint-button-secondary--light'); 
				    },
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light');
				    	$(this).addClass('mint-button-secondary--light-inverse');  
				    }
				)
			});

	    	function showModal() {
	    		if (!flag) {
    				$('#myModal').modal('show');
    				flag = true;
    			}
	    	}

	    </script>

	    <style>
			.custom-h1 { margin-bottom: 25px; text-align: center; }
			.custom-button { height:50px; color: white; width: 100%; font-size: 20px; background-color: #ff782e; box-shadow: inset 0 -3px 0 orangered; }
			.registro_mssg { margin-top: 20px; font-size: 15px; text-align: center; color:black; }
			.custom-error-mssg { color: red; font-size:14px; }
			#errors { border:solid 1px #E58E8E; padding:10px; margin:25px 0px; display:block; width:437px; -webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px; background-color:#FFE6E6; display:none; }
			#errors.visible { display:block; }
			#success { border:solid 1px #458B00; padding:10px; margin:25px 0px; display:block; width:437px; -webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px; background-color:#BCED91; display:none;  }
			#success.visible { display:block; }

			.mint-hero__button {
				margin-right: 12px;
			}

			.mint-button-secondary {
				text-decoration: none;
			    display: inline-block;
			    /*overflow: hidden;*/
			    line-height: 1.5rem;
			    letter-spacing: .03rem;
			    cursor: pointer;
			    border-radius: 2rem;
			    height: 3rem;
			    padding: 0 .99975rem;
			    border-color: #6ed6a0;
			    color: #6ed6a0!important;
			    fill: #6ed6a0;
			    border-width: 2px;
			    border-style: solid;
			    background-color: transparent;
			    transition: background-color .3s ease-out,color .3s ease-out,fill .3s ease-out;

			    text-overflow: ellipsis;
			    min-height: 1.5rem;
			    margin: 0;
			    position: relative;
			    font-size: .95rem;

			    text-transform: uppercase;
	    		font-weight: 700;
			}

			.mint-button-secondary--light-inverse {
			    border-color: #fff;
			    color: black!important;
			    fill: #000;
			    background-color: #fff;
			}

			.mint-button-secondary--light {
			    border-color: #fff;
			    color: white!important;
			    fill: #fff;
			    background-color: transparent;
			}

			.mint-button-secondary__hole {
			    display: -webkit-flex;
			    display: -ms-flexbox;
			    display: flex;
			    -webkit-flex-wrap: wrap;
			    -ms-flex-wrap: wrap;
			    flex-wrap: wrap;
			    -webkit-align-items: center;
			    -ms-flex-align: center;
			    align-items: center;
			    height: 100%;
			    -webkit-justify-content: center;
			    -ms-flex-pack: center;
			    justify-content: center;
			}
		</style>
	</head>
	<body <?php echo body_class('cbp-spmenu-push') ?>>

		<?php
			global $current_user;
			$site_logo = ae_get_option('site_logo');
		?>

		<!-- Modal -->
		<div id="myModal" class="modal fade" role="dialog">
		  <div class="modal-dialog">
		    <!-- Modal content-->
		    <div class="modal-content">
		      <div class="modal-header">
		        <button type="button" class="close" data-dismiss="modal">&times;</button>
		        <h4 class="modal-title text-center" style="font-size:20px"><strong>¡Regístrate y disfruta de las ventajas de ser miembro!</strong></h4>
		      </div>
		      <div class="modal-body">
		        <p style="text-align:justify; font-size:17px;">Las preguntas anónimas son una manera rápida de preguntar tu duda sin necesidad de registrarse antes, pero tienen varias desventajas. Por ejemplo, no puedes adjuntar imágenes en tu pregunta, tampoco conseguir puntos por ella, por lo que no podrás subir de nivel ni ayudar a otros usuarios con sus dudas :(</p>

		        <p style="text-align:justify; font-size:17px;">Regístrate ahora y benefíciate de todas las ventajas de ser miembro de Academia Abation</p>
		      </div>
		      <div class="modal-footer">
		      	<a id="modal-login" href="/login" class="btn btn-success">¡Ya tengo una cuenta!</a>
		      	<a id="modal-register" href="/register" class="btn btn-info">¡Quiero registrarme!</a>
		        <button id="modal-close" type="button" class="btn btn-default" data-dismiss="modal">No quiero conseguir puntos</button>
		      </div>
		    </div>
		  </div>
		</div>

		<div class="container-fluid">
			<div class="row">
				<header id="header">
					<div class="col-md-6 col-xs-6 text-center" id="logo">
						<a href="<?php echo home_url(); ?>">
							<?php if(!empty($site_logo)){ ?>
							<img src="<?php echo $site_logo['large'][0] ?>">
							<?php } else { ?>
							<img src="<?php echo TEMPLATEURL ?>/img/logo.png">
							<?php } ?>
						</a>
					</div><!-- logo -->
					<div id="login_qa" style="line-height:80px" class="col-md-6 col-xs-6 text-center btn-group">
						<!--<p style="color:white">No tengo mi cuenta todavía, quiero <a href="../register/">registrarme</a>.</p>-->
						<a id="custom-login-button" class="mint-button-secondary mint-button-secondary--light mint-hero__button" data-track="click_log_in_link" href="/login">
					        <div class="mint-button-secondary__hole">Entrar</div>
					    </a>

					    <a id="custom-register-button" class="mint-button-secondary mint-button-secondary--light-inverse mint-hero__button" data-track="click_register_link" href="/register">
					        <div class="mint-button-secondary__hole">Regístrate</div>
					    </a>
					</div><!-- login/register -->
				</header><!-- END HEADER -->

				<div class="container">
					<h1 class="custom-h1">Pregunta tu duda ahora de manera anónima</h1>
					<div class="row">
						<div class="col-md-6 col-md-offset-3">

							<?php
							    //init variables
							    $error = false;
							    $success = false;
							     
							    if(isset($_SESSION['errors'])) {
							        $error = true;
								}
								elseif(isset($_SESSION['success'])) {
									$success = true;
								}
							?>

							<ul id="errors" class="<?php echo ($error) ? 'visible' : ''; ?>">
						        <li id="info"><i class="fa fa-times"></i> Oops, parece que ha habido algún problema:</li>
						        <?php 
						        if(isset($_SESSION['errors']) && count($_SESSION['errors']) > 0) :
						            foreach($_SESSION['errors'] as $error) :
						        ?>
						        <li>- <?php echo $error ?></li>
						        <?php
						            endforeach;
						        endif;
						        ?>
						    </ul>

							<form id="mainForm" accept-charset="UTF-8" method="POST" action="<?php echo add_query_arg('do', 'anonymous-question', get_permalink( $post->ID )); ?>" role="form">
							 	<div class="form-group">
							 		<label for="question_title" id="question_title_error" class="custom-error-mssg" style="display: none;">Debes escribir un título para tu pregunta</label>
							    	<input required type="text" class="form-control" id="question_title" name="question_title" onfocus="onFocusQuestionTitle();" placeholder="Escribe un título para tu pregunta">
							  	</div>

							  	<!--<div class="form-group">
							 		<label for="question_category" id="question_category" class="custom-error-mssg" style="display: none;">Debes elegir una categoría para tu pregunta</label>
					  				<select required id="question_category" name="question_category" class="form-control" onfocus="onFocusQuestionCategory();">
					  					<option selected="selected" value="0">Categoría de la pregunta</option>
					  					<option value="matematicas">Matemáticas</option>
										<option value="ingles">Inglés</option>
									</select>
							  	</div>-->
							  	<?php qa_custom_select_categories() ?>

							  	<div class="form-group">
							 		<label for="question_content" id="question_content_error" class="custom-error-mssg" style="display: none;">Descripción inválida</label>
							    	<textarea id="question_content" name="question_content" class="form-control" rows="10" placeholder="Aquí puedes añadir información adicional sobre tu pregunta si lo necesitas"></textarea>
							  	</div>
							  	<p class="condiciones_mssg">Al hacer clic en el botón inferior aceptas las <a target="_blank" href="/term/">Condiciones de uso</a>.</p>

							  	<button type="submit" class="btn custom-button">Enviar pregunta anónimamente</button>

							  	<input type="hidden" name="abation_anonymous_question_nonce" value="<?php echo wp_create_nonce('abation-anonymous-question-nonce'); ?>"/>

							  	<p style="margin-top:30px; color:#bbbdbf; text-align:left; font-size: 2.8125rem; line-height: 4.0005rem; text-transform: uppercase; letter-spacing: .0625rem;">o disfruta de todas las ventajas <a href="/register/">registrándote ahora</a></p>

							  	<!--<p class="registro_mssg">o vuelve a <a href="../login/">la página de inició de sesión</a>.</p>-->
							</form>
							<?php 
								unset($_SESSION['errors']);
								unset($_SESSION['success']);
							 ?>
						</div>
					</div>
				</div>


<?php get_footer() ?>
