<?php
/**
 * Template Name: Custom Register
 * version 1.0
 * @author: Steven Máñez - Heredero al trono de Isengard (Abation)
 **/
?>

<?php
	if (is_user_logged_in()) {
	    wp_redirect( home_url() ); 
	    exit;
	} 
?>

<?php session_start() ?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="utf-8">
        <?php
			$options = AE_Options::get_instance();
			$favicon = $options->mobile_icon['thumbnail'][0];
        ?>
        <title><?php wp_title(); ?></title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="shortcut icon" href="<?php echo $favicon ?>"/>
		<link href='//fonts.googleapis.com/css?family=Lato:400,700&subset=latin,cyrillic,cyrillic-ext,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/libs/selectivizr-min.js"></script>
		<?php
		    //loads comment reply JS on single posts and pages
		    if ( is_single()) wp_enqueue_script( 'comment-reply' );
	    ?>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	    <?php wp_head(); ?>

	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/myFormScripts/custom-register.js"></script>

	    <script>
			$(document).ready(function() {
				$('#custom-login-button').hover(
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light');
				    	$(this).addClass('mint-button-secondary--light-inverse'); 
				    },
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light-inverse'); 
				    	$(this).addClass('mint-button-secondary--light'); 
				    }
				)
			});
		</script>

	    <style>
			.custom-h1 { margin-bottom: 25px; text-align: center; }
			.custom-h3 {margin-bottom: 20px;}
			.custom-button { color: white; height:50px; width: 100%; font-size: 16px; background-color: #ff782e; box-shadow: inset 0 -3px 0 orangered;display: inline-block;
			margin-bottom: 0;   font-weight: 400;    text-align: center;    vertical-align: middle;    cursor: pointer;    background-image: none;    border: 1px solid transparent;    white-space: nowrap;
			padding: 6px 12px;    /* font-size: 14px; */    line-height: 1.42857143;    border-radius: 4px;    -webkit-user-select: none; }
			.custom-h2 { font-size: 18px; text-align: center; margin-bottom: 20px; font-style: italic; }
			.registro_mssg { margin-top: 20px; font-size: 15px; text-align: center; color:black; }
			.custom-error-mssg { color: red; font-size:14px; }
			.custom-facebook {  color: white; width: 100%; height:50px; font-size: 16px; background-color: #5872a7; box-shadow: inset 0 -3px 0 #344463; display: inline-block;
			margin-bottom: 0;    font-weight: 400;    text-align: center;    vertical-align: middle;    cursor: pointer;    background-image: none;    border: 1px solid transparent;    white-space: nowrap;
			padding: 6px 12px;    /* font-size: 14px; */    line-height: 1.42857143;    border-radius: 4px;    -webkit-user-select: none; }
			#errors { border:solid 1px #E58E8E; padding:10px; margin:25px 0px; display:block; width:437px; -webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px; background-color:#FFE6E6; display:none; }
			#errors.visible { display:block; }

			.mint-hero__button {
				margin-right: 12px;
			}

			.mint-button-secondary {
				text-decoration: none;
			    display: inline-block;
			    /*overflow: hidden;*/
			    line-height: 1.5rem;
			    letter-spacing: .03rem;
			    cursor: pointer;
			    border-radius: 2rem;
			    height: 3rem;
			    padding: 0 .99975rem;
			    border-color: #6ed6a0;
			    color: #6ed6a0!important;
			    fill: #6ed6a0;
			    border-width: 2px;
			    border-style: solid;
			    background-color: transparent;
			    transition: background-color .3s ease-out,color .3s ease-out,fill .3s ease-out;

			    text-overflow: ellipsis;
			    min-height: 1.5rem;
			    margin: 0;
			    position: relative;
			    font-size: .95rem;

			    text-transform: uppercase;
	    		font-weight: 700;
			}

			.mint-button-secondary--light-inverse {
			    border-color: #fff;
			    color: black!important;
			    fill: #000;
			    background-color: #fff;
			}

			.mint-button-secondary--light {
			    border-color: #fff;
			    color: white!important;
			    fill: #fff;
			    background-color: transparent;
			}

			.mint-button-secondary__hole {
			    display: -webkit-flex;
			    display: -ms-flexbox;
			    display: flex;
			    -webkit-flex-wrap: wrap;
			    -ms-flex-wrap: wrap;
			    flex-wrap: wrap;
			    -webkit-align-items: center;
			    -ms-flex-align: center;
			    align-items: center;
			    height: 100%;
			    -webkit-justify-content: center;
			    -ms-flex-pack: center;
			    justify-content: center;
			}

		</style>
	</head>
	<body <?php echo body_class('cbp-spmenu-push') ?>>
		<div class="sampleClass"></div>
    	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
			<?php get_sidebar( 'left-tablet' ); ?>
		</nav>
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
			<?php get_sidebar( 'right-tablet' ); ?>
		</nav>

		<nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top" id="cbp-spmenu-s3">
			<ul class="menu-res">
				<?php
					if(has_nav_menu('et_header')){
						wp_nav_menu(array(
							'theme_location' => 'et_header',
							'items_wrap'     => '%3$s',
							'container'      => ''
						));
					}
				?>
			</ul>
		</nav>

		<?php
			global $current_user;
			$site_logo = ae_get_option('site_logo');
		?>
		<div class="container-fluid">
			<div class="row">
				<header id="header">
					<div class="col-md-6 col-xs-6 text-center" id="logo">
						<a href="<?php echo home_url(); ?>">
							<?php if(!empty($site_logo)){ ?>
							<img src="<?php echo $site_logo['large'][0] ?>">
							<?php } else { ?>
							<img src="<?php echo TEMPLATEURL ?>/img/logo.png">
							<?php } ?>
						</a>
					</div><!-- logo -->
					<div id="login_qa" style="line-height:80px; padding-top:0px;" class="col-md-6 col-xs-6 text-center btn-group">
						<!--<p style="color:white">Ya tengo mi cuenta, <a href="../login/">inicia la sesión</a>.</p>-->
						<a id="custom-login-button" class="mint-button-secondary mint-button-secondary--light mint-hero__button" data-track="click_log_in_link" href="/login">
					        <div class="mint-button-secondary__hole">Entrar</div>
					    </a>
					</div><!-- login/registro -->
				</header><!-- END HEADER -->

		<div class="container">
			<h1 class="custom-h1">Únete a nosotros y comparte tu conocimiento</h1>
			<div class="row">
				<div class="col-md-4 col-md-offset-4">
					
					<?php
					    //init variables
					    $cf = array();
					    $sr = false;
					     
					    if(isset($_SESSION['cf_returndata'])) {
					        $cf = $_SESSION['cf_returndata'];
					        $sr = true;
						}
					?>

				    <ul id="errors" class="<?php echo ($sr) ? 'visible' : ''; ?>">
				        <li id="info">Oops, parece que ha habido algún problema:</li>
				        <?php 
				        if(isset($cf['errors']) && count($cf['errors']) > 0) :
				            foreach($cf['errors'] as $error) :
				        ?>
				        <li>- <?php echo $error ?></li>
				        <?php
				            endforeach;
				        endif;
				        ?>
				    </ul>

					<form id="mainForm" method="POST" action="<?php echo add_query_arg('do', 'register', get_permalink( $post->ID )); ?>" role="form">
					<div title="Facebook All"><span id="fball-facebook-login">
								<input type = 'button' class="custom-facebook" onclick = "FbAll.facebookLogin();" value = 'Con Facebook'>
								<div id="fball_facebook_auth">
								  <input type="hidden" name="client_id" id="client_id" value="1120411377986549"/>
								  <input type="hidden" name="redirect_uri" id="redirect_uri" value="http://academia.abation.es"/>
								</div>
								<input type="hidden" id="fball_login_form_uri" value=""/>
							</div>
							<h3 class="custom-h3">O</h3>
					 	<div class="form-group">
					 		<label for="user_email" id="user_email_error" class="custom-error-mssg" style="display: none;">Email inválido</label>
					    	<input required value="<?php echo ($sr) ? $cf['posted_form_data']['user_email'] : '' ?>" type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" onfocus="onFocusEmail();">
					  	</div>
					  	<div class="form-group">
					  		<label for="user_nickname" id="user_nickname_error" class="custom-error-mssg" style="display: none;">El nombre de usuario debe tener al menos 4 caracteres</label>
					    	<input required minlength="4" value="<?php echo ($sr) ? $cf['posted_form_data']['user_nickname'] : '' ?>" type="text" class="form-control" id="user_nickname" name="user_nickname" placeholder="Nombre de usuario" onfocus="onFocusNickname();">
					  	</div>
					  	<div class="form-group">
					  		<label for="user_password" id="user_password_error" class="custom-error-mssg" style="display: none;">La contraseña debe tener al menos 6 caracteres</label>
					    	<input required minlength="6" type="password" class="form-control" id="user_password" name="user_password" placeholder="Contraseña (mínimo 6 caracteres)" onfocus="onFocusPassword();">
					  	</div>
					  	<div class="form-group">
					  		<label for="user_sex" id="user_sex_error" class="custom-error-mssg" style="display: none;">Debes elegir una opción</label>
					  		<div class="radio">
						  		<label><input required <?php if($sr && $cf['posted_form_data']['user_sex'] == "female") echo "checked"; ?> type="radio" id="user_sex" name="user_sex" value="female" onfocus="onFocusSex();">Mujer</label>
							</div>
							<div class="radio">
							  <label><input <?php if($sr && $cf['posted_form_data']['user_sex'] == "male") echo "checked"; ?>  type="radio" id="user_sex" name="user_sex" value="male" onfocus="onFocusSex();">Hombre</label>
							</div>
					  	</div>
					  	<div class="form-group">
					  		<label for="user_birth_year" id="user_birth_year_error" class="custom-error-mssg" style="display: none;">Debes elegir una opción</label>
					  		<select required id="user_birth_year" name="user_birth_year" class="form-control" onfocus="onFocusBirthYear();">
					  			<option <?php if (!$sr) echo 'selected="selected"'; ?> value="0">Año de nacimiento</option>
					  			<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2010") echo 'selected="selected"'; ?> value="2010">2010</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2009") echo 'selected="selected"'; ?> value="2009">2009</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2008") echo 'selected="selected"'; ?> value="2008">2008</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2007") echo 'selected="selected"'; ?> value="2007">2007</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2006") echo 'selected="selected"'; ?> value="2006">2006</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2005") echo 'selected="selected"'; ?> value="2005">2005</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2004") echo 'selected="selected"'; ?> value="2004">2004</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2003") echo 'selected="selected"'; ?> value="2003">2003</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2002") echo 'selected="selected"'; ?> value="2002">2002</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2001") echo 'selected="selected"'; ?> value="2001">2001</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2000") echo 'selected="selected"'; ?> value="2000">2000</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1999") echo 'selected="selected"'; ?> value="1999">1999</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1998") echo 'selected="selected"'; ?> value="1998">1998</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1997") echo 'selected="selected"'; ?> value="1997">1997</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1996") echo 'selected="selected"'; ?> value="1996">1996</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1995") echo 'selected="selected"'; ?> value="1995">1995</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1994") echo 'selected="selected"'; ?> value="1994">1994</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1993") echo 'selected="selected"'; ?> value="1993">1993</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1992") echo 'selected="selected"'; ?> value="1992">1992</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1991") echo 'selected="selected"'; ?> value="1991">1991</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1990") echo 'selected="selected"'; ?> value="1990">1990</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1989") echo 'selected="selected"'; ?> value="1989">1989</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1988") echo 'selected="selected"'; ?> value="1988">1988</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1987") echo 'selected="selected"'; ?> value="1987">1987</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1986") echo 'selected="selected"'; ?> value="1986">1986</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1985") echo 'selected="selected"'; ?> value="1985">1985</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1984") echo 'selected="selected"'; ?> value="1984">1984</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1983") echo 'selected="selected"'; ?> value="1983">1983</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1982") echo 'selected="selected"'; ?> value="1982">1982</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1981") echo 'selected="selected"'; ?> value="1981">1981</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1980") echo 'selected="selected"'; ?> value="1980">1980</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1979") echo 'selected="selected"'; ?> value="1979">1979</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1978") echo 'selected="selected"'; ?> value="1978">1978</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1977") echo 'selected="selected"'; ?> value="1977">1977</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1976") echo 'selected="selected"'; ?> value="1976">1976</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1975") echo 'selected="selected"'; ?> value="1975">1975</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1974") echo 'selected="selected"'; ?> value="1974">1974</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1973") echo 'selected="selected"'; ?> value="1973">1973</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1972") echo 'selected="selected"'; ?> value="1972">1972</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1971") echo 'selected="selected"'; ?> value="1971">1971</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1970") echo 'selected="selected"'; ?> value="1970">1970</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1969") echo 'selected="selected"'; ?> value="1969">1969</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1968") echo 'selected="selected"'; ?> value="1968">1968</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1967") echo 'selected="selected"'; ?> value="1967">1967</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1966") echo 'selected="selected"'; ?> value="1966">1966</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1965") echo 'selected="selected"'; ?> value="1965">1965</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1964") echo 'selected="selected"'; ?> value="1964">1964</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1963") echo 'selected="selected"'; ?> value="1963">1963</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1962") echo 'selected="selected"'; ?> value="1962">1962</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1961") echo 'selected="selected"'; ?> value="1961">1961</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1960") echo 'selected="selected"'; ?> value="1960">1960</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1959") echo 'selected="selected"'; ?> value="1959">1959</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1958") echo 'selected="selected"'; ?> value="1958">1958</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1957") echo 'selected="selected"'; ?> value="1957">1957</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1956") echo 'selected="selected"'; ?> value="1956">1956</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1955") echo 'selected="selected"'; ?> value="1955">1955</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1954") echo 'selected="selected"'; ?> value="1954">1954</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1953") echo 'selected="selected"'; ?> value="1953">1953</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1952") echo 'selected="selected"'; ?> value="1952">1952</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1951") echo 'selected="selected"'; ?> value="1951">1951</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1950") echo 'selected="selected"'; ?> value="1950">1950</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1949") echo 'selected="selected"'; ?> value="1949">1949</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1948") echo 'selected="selected"'; ?> value="1948">1948</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1947") echo 'selected="selected"'; ?> value="1947">1947</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1946") echo 'selected="selected"'; ?> value="1946">1946</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1945") echo 'selected="selected"'; ?> value="1945">1945</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1944") echo 'selected="selected"'; ?> value="1944">1944</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1943") echo 'selected="selected"'; ?> value="1943">1943</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1942") echo 'selected="selected"'; ?> value="1942">1942</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1941") echo 'selected="selected"'; ?> value="1941">1941</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1940") echo 'selected="selected"'; ?> value="1940">1940</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1939") echo 'selected="selected"'; ?> value="1939">1939</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1938") echo 'selected="selected"'; ?> value="1938">1938</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1937") echo 'selected="selected"'; ?> value="1937">1937</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1936") echo 'selected="selected"'; ?> value="1936">1936</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1935") echo 'selected="selected"'; ?> value="1935">1935</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1934") echo 'selected="selected"'; ?> value="1934">1934</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1933") echo 'selected="selected"'; ?> value="1933">1933</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1932") echo 'selected="selected"'; ?> value="1932">1932</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1931") echo 'selected="selected"'; ?> value="1931">1931</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1930") echo 'selected="selected"'; ?> value="1930">1930</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1929") echo 'selected="selected"'; ?> value="1929">1929</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1928") echo 'selected="selected"'; ?> value="1928">1928</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1927") echo 'selected="selected"'; ?> value="1927">1927</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1926") echo 'selected="selected"'; ?> value="1926">1926</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1925") echo 'selected="selected"'; ?> value="1925">1925</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1924") echo 'selected="selected"'; ?> value="1924">1924</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1923") echo 'selected="selected"'; ?> value="1923">1923</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1922") echo 'selected="selected"'; ?> value="1922">1922</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1921") echo 'selected="selected"'; ?> value="1921">1921</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1920") echo 'selected="selected"'; ?> value="1920">1920</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1919") echo 'selected="selected"'; ?> value="1919">1919</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1918") echo 'selected="selected"'; ?> value="1918">1918</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1917") echo 'selected="selected"'; ?> value="1917">1917</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1916") echo 'selected="selected"'; ?> value="1916">1916</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1915") echo 'selected="selected"'; ?> value="1915">1915</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1914") echo 'selected="selected"'; ?> value="1914">1914</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1913") echo 'selected="selected"'; ?> value="1913">1913</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1912") echo 'selected="selected"'; ?> value="1912">1912</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1911") echo 'selected="selected"'; ?> value="1911">1911</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1910") echo 'selected="selected"'; ?> value="1910">1910</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1909") echo 'selected="selected"'; ?> value="1909">1909</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1908") echo 'selected="selected"'; ?> value="1908">1908</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1907") echo 'selected="selected"'; ?> value="1907">1907</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1906") echo 'selected="selected"'; ?> value="1906">1906</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1905") echo 'selected="selected"'; ?> value="1905">1905</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1904") echo 'selected="selected"'; ?> value="1904">1904</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1903") echo 'selected="selected"'; ?> value="1903">1903</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1902") echo 'selected="selected"'; ?> value="1902">1902</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1901") echo 'selected="selected"'; ?> value="1901">1901</option>
								<option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1900") echo 'selected="selected"'; ?> value="1900">1900</option>
					  		</select>
					  	</div>
					  	<div class="form-group">
					  		<label for="user_grade" id="user_grade_error" class="custom-error-mssg" style="display: none;">Debes elegir una opción</label>
					  		<select required id="user_grade" name="user_grade" class="form-control" onfocus="onFocusGrade();">
					  			<option <?php if (!$sr) echo 'selected="selected"'; ?> value="0">Elije tu nivel</option>
			                	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "primaria") echo 'selected="selected"'; ?> value="primaria">Primaria</option>
			                	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "secundaria") echo 'selected="selected"'; ?> value="secundaria">Secundaria</option>
			                 	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "bachillerato") echo 'selected="selected"'; ?> value="bachillerato">Bachillerato</option>
			                 	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "cfgm") echo 'selected="selected"'; ?> value="cfgm">Ciclo formativo grado medio</option>
			                 	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "cfgs") echo 'selected="selected"'; ?> value="cfgs">Ciclo formativo grado superior</option>
			                 	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "grado_universitario") echo 'selected="selected"'; ?> value="grado_universitario">Grado Universitario</option>
			                 	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "master_universitario") echo 'selected="selected"'; ?> value="master_universitario">Master Universitario</option>
			                 	<option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "doctorado") echo 'selected="selected"'; ?> value="doctorado">Doctorado</option>
					  		</select>
					  	</div>
					  	<p class="condiciones_mssg">Al hacer clic en el botón inferior aceptas las <a target="_blank" href="../term/">Condiciones de uso</a>.</p>
					  	<button type="submit" class="custom-button">Terminado</button>

					  	<input type="hidden" id="abation_register_nonce" name="abation_register_nonce" value="<?php echo wp_create_nonce('abation-register-nonce'); ?>"/>

					  	<p class="inicia_sesion_mssg">Ya tengo mi cuenta, <a href="../login/">inicia la sesión</a>.</p>
					</form>
					<?php unset($_SESSION['cf_returndata']); ?>
				</div>
			</div>
		</div>


<?php get_footer() ?>
