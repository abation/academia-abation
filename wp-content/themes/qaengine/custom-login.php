<?php
/**
 * Template Name: Custom Login
 * version 1.0
 * @author: Steven Máñez - Heredero al trono de Isengard (Abation)
 **/
?>


<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="utf-8">
        <?php
			$options = AE_Options::get_instance();
			$favicon = $options->mobile_icon['thumbnail'][0];
        ?>
        <title><?php wp_title(); ?></title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="shortcut icon" href="<?php echo $favicon ?>"/>
		<link href='//fonts.googleapis.com/css?family=Lato:400,700&subset=latin,cyrillic,cyrillic-ext,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/libs/selectivizr-min.js"></script>
		
		<?php
		    //loads comment reply JS on single posts and pages
		    if ( is_single()) wp_enqueue_script( 'comment-reply' );
	    ?>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	    <?php wp_head(); ?>

	    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
	    <script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/myFormScripts/custom-login.js"></script>

	    <style>
			.custom-h1 { margin-bottom: 25px; text-align: center; }
			.custom-h3 {margin-bottom: 20px;}
			.custom-button { color: white; height:50px; width: 100%; font-size: 16px; background-color: #ff782e; box-shadow: inset 0 -3px 0 orangered;display: inline-block;
			margin-bottom: 0;   font-weight: 400;    text-align: center;    vertical-align: middle;    cursor: pointer;    background-image: none;    border: 1px solid transparent;    white-space: nowrap;
			padding: 6px 12px;    /* font-size: 14px; */    line-height: 1.42857143;    border-radius: 4px;    -webkit-user-select: none; }
			.custom-h2 { font-size: 18px; text-align: center; margin-bottom: 20px; font-style: italic; }
			.registro_mssg { margin-top: 20px; font-size: 15px; text-align: center; color:black; }
			.custom-error-mssg { color: red; font-size:14px; }
			.custom-facebook {  color: white; width: 100%; height:50px; font-size: 16px; background-color: #5872a7; box-shadow: inset 0 -3px 0 #344463; display: inline-block;
			margin-bottom: 0;    font-weight: 400;    text-align: center;    vertical-align: middle;    cursor: pointer;    background-image: none;    border: 1px solid transparent;    white-space: nowrap;
			padding: 6px 12px;    /* font-size: 14px; */    line-height: 1.42857143;    border-radius: 4px;    -webkit-user-select: none; }
			#errors { border:solid 1px #E58E8E; padding:10px; margin:25px 0px; display:block; width:437px; -webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px; background-color:#FFE6E6; display:none; }
			#errors.visible { display:block; }

			.mint-hero__button {
				margin-right: 12px;
			}

			.mint-button-secondary {
				text-decoration: none;
			    display: inline-block;
			    /*overflow: hidden;*/
			    line-height: 1.5rem;
			    letter-spacing: .03rem;
			    cursor: pointer;
			    border-radius: 2rem;
			    height: 3rem;
			    padding: 0 .99975rem;
			    border-color: #6ed6a0;
			    color: #6ed6a0!important;
			    fill: #6ed6a0;
			    border-width: 2px;
			    border-style: solid;
			    background-color: transparent;
			    transition: background-color .3s ease-out,color .3s ease-out,fill .3s ease-out;

			    text-overflow: ellipsis;
			    min-height: 1.5rem;
			    margin: 0;
			    position: relative;
			    font-size: .95rem;

			    text-transform: uppercase;
	    		font-weight: 700;
			}

			.mint-button-secondary--light-inverse {
			    border-color: #fff;
			    color: black!important;
			    fill: #000;
			    background-color: #fff;
			}

			.mint-button-secondary--light {
			    border-color: #fff;
			    color: white!important;
			    fill: #fff;
			    background-color: transparent;
			}

			.mint-button-secondary__hole {
			    display: -webkit-flex;
			    display: -ms-flexbox;
			    display: flex;
			    -webkit-flex-wrap: wrap;
			    -ms-flex-wrap: wrap;
			    flex-wrap: wrap;
			    -webkit-align-items: center;
			    -ms-flex-align: center;
			    align-items: center;
			    height: 100%;
			    -webkit-justify-content: center;
			    -ms-flex-pack: center;
			    justify-content: center;
			}

			.btn-facebook, .btn-facebook:visited {
			    border-color: #29447e;
			    border-bottom-color: #1a356e;
			    color: #fff;
			    background-color: #5872a7;
			    background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#637bad), to(#5872a7));
			    background-image: -webkit-linear-gradient(#637bad, #5872a7);
			    background-image: -moz-linear-gradient(#637bad, #5872a7);
			    background-image: -ms-linear-gradient(#637bad, #5872a7);
			    background-image: -o-linear-gradient(#637bad, #5872a7);
			    background-image: linear-gradient(#637bad, #5872a7);
			    -webkit-box-shadow: inset 0 1px 0 #879ac0;
			    box-shadow: inset 0 1px 0 #879ac0;
			}

			.btn-auth.large {
			    height: 36px;
			    line-height: 36px;
			    font-size: 20px;
			}

			.btn-auth, .btn-auth:visited {
			    position: relative;
			    display: inline-block;
			    height: 22px;
			    padding: 0 1em;
			    border: 1px solid #999;
			    border-radius: 2px;
			    margin: 0;
			    text-align: center;
			    text-decoration: none;
			    font-size: 14px;
			    line-height: 22px;
			    white-space: nowrap;
			    cursor: pointer;
			    color: #222;
			    background: #fff;
			    -webkit-box-sizing: content-box;
			    -moz-box-sizing: content-box;
			    box-sizing: content-box;
			    -webkit-user-select: none;
			    -moz-user-select: none;
			    -ms-user-select: none;
			    user-select: none;
			    -webkit-appearance: none;
			}

		</style>

		<script>
			$(document).ready(function() {
				$('#custom-register-button').hover(
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light-inverse'); 
				    	$(this).addClass('mint-button-secondary--light'); 
				    },
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light');
				    	$(this).addClass('mint-button-secondary--light-inverse');  
				    }
				)
			});
		</script>
	</head>
	<body <?php echo body_class('cbp-spmenu-push') ?>>

		<?php
			global $current_user;
			$site_logo = ae_get_option('site_logo');
		?>

		<div class="container-fluid">
			<div class="row">
				<header id="header">
					<div class="col-md-6 col-xs-6 text-center" id="logo">
						<a href="<?php echo home_url(); ?>">
							<?php if(!empty($site_logo)){ ?>
							<img src="<?php echo $site_logo['large'][0] ?>">
							<?php } else { ?>
							<img src="<?php echo TEMPLATEURL ?>/img/logo.png">
							<?php } ?>
						</a>
					</div><!-- logo -->
					<div id="login_qa" style="line-height:80px" class="col-md-6 col-xs-6 text-center btn-group">
						<!--<p style="color:white">No tengo mi cuenta todavía, quiero <a href="../register/">registrarme</a>.</p>-->
						<a id="custom-register-button" class="mint-button-secondary mint-button-secondary--light-inverse mint-hero__button" data-track="click_register_link" href="/register">
					        <div class="mint-button-secondary__hole">Regístrate</div>
					    </a>
					</div><!-- login/register -->
				</header><!-- END HEADER -->

				<div class="container">
					<h1 class="custom-h1">Inicia sesión y empieza a divertirte aprendiendo</h1>
					<h2 id="customized-info-message" class="custom-h2" style="display:none"></h2>
					<div class="row">
						<div class="col-md-4 col-md-offset-4">

							<?php
							    //init variables
							    $cf = array();
							    $sr = false;
							     
							    if(isset($_SESSION['cf_returndata'])) {
							        $cf = $_SESSION['cf_returndata'];
							        $sr = true;
								}
							?>

						    <ul id="errors" class="<?php echo ($sr) ? 'visible' : ''; ?>">
						        <li id="info">Oops, parece que ha habido algún problema:</li>
						        <?php 
						        if(isset($cf['errors']) && count($cf['errors']) > 0) :
						            foreach($cf['errors'] as $error) :
						        ?>
						        <li>- <?php echo $error ?></li>
						        <?php
						            endforeach;
						        endif;
						        ?>
						    </ul>


							<form id="mainForm" accept-charset="UTF-8" method="POST" action="<?php echo add_query_arg('do', 'login', get_permalink( $post->ID )); ?>" role="form">
								<div title="Facebook All"><span id="fball-facebook-login">
								<input type = 'button' class="custom-facebook" onclick = "FbAll.facebookLogin();" value = 'Con Facebook'>
								<div id="fball_facebook_auth">
								  <input type="hidden" name="client_id" id="client_id" value="1120411377986549"/>
								  <input type="hidden" name="redirect_uri" id="redirect_uri" value="http://academia.abation.es"/>
								</div>
								<input type="hidden" id="fball_login_form_uri" value=""/>
							</div>
							<h3 class="custom-h3">O</h3>
								
							 	<div class="form-group">
							 		<label for="user_email" id="user_email_error" class="custom-error-mssg" style="display: none;">Email inválido</label>
							    	<input required value="<?php echo ($sr) ? $cf['posted_form_data']['user_email'] : '' ?>" type="email" class="form-control" id="user_email" name="user_email" onfocus="onFocusEmail();" placeholder="Email">
							  	</div>
							  	<div class="form-group">
							  		<label for="user_password" id="user_password_error" class="custom-error-mssg" style="display: none;">Contraseña inválida</label>
							    	<input required type="password" class="form-control" id="user_password" name="user_password" onfocus="onFocusPassword();" placeholder="Contraseña">
							  	</div>

							  	<div class="checkbox">
						          <label>
						            <input id="rememberme" style="color:black" type="checkbox" value="remember-me" checked> No cerrar sesión
						            <a id="forgot_password_link" href="../forgot-password/" style="float:right">¿Has olvidado tu contraseña?</a>
						          </label>
						        </div>

							  	<button type="submit" class="custom-button">Iniciar sesión</button>

							  	<input type="hidden" name="abation_login_nonce" value="<?php echo wp_create_nonce('abation-login-nonce'); ?>"/>
								<div class="fball_ui">

							  	<p id="register_mssg_bottom" class="registro_mssg">No tengo mi cuenta todavía, quiero <a href="../register/">registrarme</a>.</p>
							</form>
							<div id="thanks_info" style="display:none; margin-top:10px;">
								<p class="thanks_text">¡Gracias por registrarte! Tus datos de acceso son los siguientes, te los hemos enviado también a tu email: </p>

								<div style="text-align:left; border-style:solid; border-color:black; border-width:1px; padding:10px; margin-bottom:5px">
									<p> Email: <strong><span id="email_response"></span></strong></p>
									<p style="margin-bottom:0px"> Contraseña (provisional): <strong><span id="password_response"></span></strong></p>
								</div>
							</div>



<?php get_footer() ?>
