<?php
/**
 * Template Name: Intro Page Template
 * version 1.0
 * @author: enginethemes
 **/
$disabled_register = is_multisite() ? get_site_option('registration') : get_option('users_can_register');
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=8">
		<meta charset="utf-8">
	    <title>
            <?php
                if(is_home() || is_front_page()) {
                    echo get_option("blogdescription").' | '.get_option("blogname") ;
                } else {
                    wp_title( '|', true, 'right' );
                }
            ?>
	    </title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<meta name="description" content="<?php echo get_option("blogdescription"); ?>">
		<?php
			$favicon = (TEMPLATEURL . '/img/fe-favicon.png');
		?>
		<link rel="shortcut icon" href="<?php echo $favicon ?>"/>
		<link href='http://fonts.googleapis.com/css?family=Lato:400,700,900' rel='stylesheet' type='text/css'>
        <link href='<?php echo TEMPLATEURL ?>/css/intro.css' rel='stylesheet' type='text/css'>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <style>
        .custom-h1 { margin-bottom: 25px; text-align: center; font-size: 30px; }
        .custom-button { color: white; height:50px; width: 100%; font-size: 20px; background-color: #ff782e; box-shadow: inset 0 -3px 0 orangered;display: inline-block;
			margin-bottom: 0;   font-weight: 400;    text-align: center;    vertical-align: middle;    cursor: pointer;    background-image: none;    border: 1px solid transparent;    white-space: nowrap;
			padding: 6px 12px;    /* font-size: 14px; */    line-height: 1.42857143;    border-radius: 4px;    -webkit-user-select: none; }
        #errors { border:solid 1px #E58E8E; padding:10px; margin:25px 0px; display:block; width:100%; -webkit-border-radius:8px; -moz-border-radius:8px; border-radius:8px; background-color:#FFE6E6; display:none; }
        #errors.visible { display:block; }
        .condiciones_mssg { font-size: 12px; margin-bottom: 15px; margin-top: 10px; }
        .custom-error-mssg { color: red; font-size:14px; }
		.custom-h3 {margin-bottom: 20px;}
		.custom-facebook { color: white; width: 100%; height:50px; font-size: 20px; background-color: #5872a7; box-shadow: inset 0 -3px 0 #344463; display: inline-block;
			margin-bottom: 0;    font-weight: 400;    text-align: center;    vertical-align: middle;    cursor: pointer;    background-image: none;    border: 1px solid transparent;    white-space: nowrap;
			padding: 6px 12px;    /* font-size: 14px; */    line-height: 1.42857143;    border-radius: 4px;    -webkit-user-select: none;}
        </style>

	    <?php wp_head(); ?>
        
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/myFormScripts/custom-register.js"></script>
        <script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/myFormScripts/custom-login.js"></script>
	</head>
	<body <?php echo body_class('intro-wrapper') ?>>
		<div class="container">
			<div class="row">
				<header id="header" class="intro-header">
					<div class="col-md-12" id="logo">
						<a href="<?php echo home_url(); ?>">
                        <?php $site_logo    =   ae_get_option('site_logo'); ?>
							<img src="<?php echo $site_logo['large'][0] ?>">
						</a>
					</div><!-- logo -->
				</header><!-- END HEADER -->

                <div class="clearfix"></div>
                <!-- CONTENT INTRO -->
                <div class="intro-content-wrapper mobile-device">
                	<div class="col-md-7">
                    	<div class="intro-text">
                            <h2 class="slide-text">
                                <?php
                                    if(ae_get_option('intro_slide_text')){
                                        $string = ae_get_option('intro_slide_text');
                                        $string = implode("",explode("\\",$string));
                                        echo stripslashes(trim($string));
                                    }
                                ?>
                                <!-- Not just another
                                <span class="adject">EngineThemes|Geek|Cool|Lazy|Nerd</span>
                                WordPress theme, -->
                            </h2>
                            <h3 class="text-bottom">
                                <?php
                                    if(ae_get_option('intro_bottom_text')){
                                        echo stripcslashes( ae_get_option('intro_bottom_text') );
                                    }
                                ?>
                                <!-- QAEngine aims to take the knowledge sharing <br>
                                experience to a <span>whole new level.</span> -->
                            </h3>
                        </div>
                    </div>
                    <!-- FORM -->
                    <div class="col-md-5">
                    	<div class="form-signup-wrapper">
                            <a class="hiddenanchor" id="toregister"></a>
                            <a class="hiddenanchor" id="tologin"></a>
                            <div class="group-btn-intro">
                                <a href="#tologin" class="to_register to_login_active active"> <?php _e("Sign in",ET_DOMAIN) ?> </a> <span><?php _e("or",ET_DOMAIN) ?></span>
                                <a href="#toregister" class="to_register to_register_active"><?php _e("Sign up",ET_DOMAIN) ?></a>
                            </div>
                            <div id="wrapper">
                                <div id="login" class="animate">

                                    <?php
                                        //init variables
                                        $cf = array();
                                        $sr = false;
                                         
                                        if(isset($_SESSION['cf_returndata'])) {
                                            $cf = $_SESSION['cf_returndata'];
                                            $sr = true;
                                        }
                                    ?>

                                    <ul id="errors" class="<?php echo ($sr) ? 'visible' : ''; ?>">
                                        <li style="list-style-type: none;" id="info">Oops, parece que ha habido algún problema:</li>
                                        <?php 
                                        if(isset($cf['errors']) && count($cf['errors']) > 0) :
                                            foreach($cf['errors'] as $error) :
                                        ?>
                                        <li style="list-style-type: none;">- <?php echo $error ?></li>
                                        <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </ul>


                                    <form id="mainForm" accept-charset="UTF-8" method="POST" action="<?php echo add_query_arg('do', 'login', get_permalink( $post->ID )); ?>" role="form">
									<div title="Facebook All"><span id="fball-facebook-login">
										<input type = 'button' class="custom-facebook" onclick = "FbAll.facebookLogin();" value = 'Con Facebook'>
									<div id="fball_facebook_auth">
										  <input type="hidden" name="client_id" id="client_id" value="1120411377986549"/>
										  <input type="hidden" name="redirect_uri" id="redirect_uri" value="http://academia.abation.es"/>
									</div>
										<input type="hidden" id="fball_login_form_uri" value=""/>
									</div>
									<h3 class="custom-h3">O</h3>
                                        <div class="form-group">
                                            <label for="user_email" id="user_email_error" class="custom-error-mssg" style="display: none;">Email inválido</label>
                                            <input style="font-size:15px; height:40px;" required value="<?php echo ($sr) ? $cf['posted_form_data']['user_email'] : '' ?>" type="email" class="form-control" id="user_email" name="user_email" onfocus="onFocusEmail();" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="user_password" id="user_password_error" class="custom-error-mssg" style="display: none;">Contraseña inválida</label>
                                            <input style="font-size:15px; height:40px;" required type="password" class="form-control" id="user_password" name="user_password" onfocus="onFocusPassword();" placeholder="Contraseña">
                                        </div>

                                        <div class="checkbox">
                                          <label>
                                            <input id="rememberme" style="color:black" type="checkbox" value="remember-me" checked> No cerrar sesión
                                            <a id="forgot_password_link" href="../forgot-password/" style="float:right">¿Has olvidado tu contraseña?</a>
                                          </label>
                                        </div>

                                        <button type="submit" class="custom-button">Iniciar sesión</button>

                                        <input type="hidden" name="abation_login_nonce" value="<?php echo wp_create_nonce('abation-login-nonce'); ?>"/>

                                        <!--<p id="register_mssg_bottom" class="registro_mssg">No tengo mi cuenta todavía, quiero <a href="../register/">registrarme</a>.</p>-->
                                    </form>
                                    <!--<form class="sign-in-intro" id="sign_in" method="POST">
                                    	<div class="row">
                                        	<div class="col-md-6">
                                            	<p class="intro-name">
                                                    <span class="your-email">
                                                        <input type="text" autocomplete="off" id="username" name="username" value="" class="" placeholder="<?php _e("Email",ET_DOMAIN) ?>">
                                                        <i class="fa fa-envelope-o"></i>
                                                    </span>
                                                </p>
                                                <p class="intro-remember collapse">
                                                    <input type="hidden" id="remember" name="remember" value="0" />
                                                	<a class="your-remember" href="javascript:void(0)">
                                                        <i class="fa fa-check-circle-o"></i> <?php _e("Remember me",ET_DOMAIN) ?>
                                                    </a>
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                            	<p class="intro-password">
                                                    <span class="your-password">
                                                        <input type="password"  autocomplete="off" id="password" name="password" class="" placeholder="<?php _e("Password",ET_DOMAIN) ?>">
                                                        <i class="fa fa-key"></i>
                                                    </span>
                                                </p>
                                                <p class="intro-remember">
                                                	<a class="your-fogot-pass collapse" href="javascript:void(0)">
                                                       <?php _e("Fogot password ?",ET_DOMAIN) ?>
                                                    </a>
                                                </p>
                                            </div>

                                            Hidden value
                                            <?php if( isset( $_GET['redirect'] ) && !empty( $_GET['redirect'] ) ) {
                                                $redirect = $_GET['redirect'];
                                            } else {
                                                $redirect = "";
                                            } ?>
                                            <input type="hidden" id="redirect" name="redirect" value="<?php echo $redirect; ?>">

                                            <div class="col-md-12">
                                            	<p class="btn-submit-intro">
                                                    <span class="your-submit mobile-device">
                                                        <input type="submit" name="" value="<?php _e("Sign in",ET_DOMAIN) ?>" class="btn-submit" />
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </form>-->
                                </div>
                                <?php if($disabled_register == 1 || $disabled_register == "user" || $disabled_register == "all" || $disabled_register == "blog" ){ ?>
                                <div id="register" class="animate ">
                                    <?php
                                        //init variables
                                        $cf = array();
                                        $sr = false;
                                         
                                        if(isset($_SESSION['cf_returndata'])) {
                                            $cf = $_SESSION['cf_returndata'];
                                            $sr = true;
                                        }
                                    ?>

                                    <ul id="errors" class="<?php echo ($sr) ? 'visible' : ''; ?>">
                                        <li style="list-style-type: none;" id="info">Oops, parece que ha habido algún problema:</li>
                                        <?php 
                                        if(isset($cf['errors']) && count($cf['errors']) > 0) :
                                            foreach($cf['errors'] as $error) :
                                        ?>
                                        <li style="list-style-type: none;">- <?php echo $error ?></li>
                                        <?php
                                            endforeach;
                                        endif;
                                        ?>
                                    </ul>


                                    <form id="mainForm" method="POST" action="<?php echo add_query_arg('do', 'register', get_permalink( $post->ID )); ?>" role="form">
									<div title="Facebook All"><span id="fball-facebook-login">
										<input type = 'button' class="custom-facebook" onclick = "FbAll.facebookLogin();" value = 'Con Facebook'>
									<div id="fball_facebook_auth">
										  <input type="hidden" name="client_id" id="client_id" value="1120411377986549"/>
										  <input type="hidden" name="redirect_uri" id="redirect_uri" value="http://academia.abation.es"/>
									</div>
										<input type="hidden" id="fball_login_form_uri" value=""/>
									</div>
									<h3 class="custom-h3">O</h3>
                                        <div class="form-group">
                                            <label for="user_email" id="user_email_error" class="custom-error-mssg" style="display: none;">Email inválido</label>
                                            <input style="font-size:15px; height:40px;" required value="<?php echo ($sr) ? $cf['posted_form_data']['user_email'] : '' ?>" type="email" class="form-control" id="user_email" name="user_email" placeholder="Email" onfocus="onFocusEmail();">
                                        </div>
                                        <div class="form-group">
                                            <label for="user_nickname" id="user_nickname_error" class="custom-error-mssg" style="display: none;">El nombre de usuario debe tener al menos 4 caracteres</label>
                                            <input style="font-size:15px; height:40px;" required minlength="4" value="<?php echo ($sr) ? $cf['posted_form_data']['user_nickname'] : '' ?>" type="text" class="form-control" id="user_nickname" name="user_nickname" placeholder="Nombre de usuario" onfocus="onFocusNickname();">
                                        </div>
                                        <div class="form-group">
                                            <label for="user_password" id="user_password_error" class="custom-error-mssg" style="display: none;">La contraseña debe tener al menos 6 caracteres</label>
                                            <input style="font-size:15px; height:40px;" required minlength="6" type="password" class="form-control" id="user_password" name="user_password" placeholder="Contraseña (mínimo 6 caracteres)" onfocus="onFocusPassword();">
                                        </div>
                                        <div class="form-group">
                                            <label for="user_sex" id="user_sex_error" class="custom-error-mssg" style="display: none;">Debes elegir una opción</label>
                                            <div class="radio">
                                                <label style="font-size:15px;"><input required <?php if($sr && $cf['posted_form_data']['user_sex'] == "female") echo "checked"; ?> type="radio" id="user_sex" name="user_sex" value="female" onclick="onFocusSex();">Mujer</label>
                                            </div>
                                            <div class="radio">
                                                <label style="font-size:15px;"><input <?php if($sr && $cf['posted_form_data']['user_sex'] == "male") echo "checked"; ?>  type="radio" id="user_sex" name="user_sex" value="male" onclick="onFocusSex();">Hombre</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_birth_year" id="user_birth_year_error" class="custom-error-mssg" style="display: none;">Debes elegir una opción</label>
                                            <select style="font-size:15px; height:40px;" required id="user_birth_year" name="user_birth_year" class="form-control" onfocus="onFocusBirthYear();">
                                                <option <?php if (!$sr) echo 'selected="selected"'; ?> value="0">Año de nacimiento</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2010") echo 'selected="selected"'; ?> value="2010">2010</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2009") echo 'selected="selected"'; ?> value="2009">2009</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2008") echo 'selected="selected"'; ?> value="2008">2008</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2007") echo 'selected="selected"'; ?> value="2007">2007</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2006") echo 'selected="selected"'; ?> value="2006">2006</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2005") echo 'selected="selected"'; ?> value="2005">2005</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2004") echo 'selected="selected"'; ?> value="2004">2004</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2003") echo 'selected="selected"'; ?> value="2003">2003</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2002") echo 'selected="selected"'; ?> value="2002">2002</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2001") echo 'selected="selected"'; ?> value="2001">2001</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "2000") echo 'selected="selected"'; ?> value="2000">2000</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1999") echo 'selected="selected"'; ?> value="1999">1999</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1998") echo 'selected="selected"'; ?> value="1998">1998</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1997") echo 'selected="selected"'; ?> value="1997">1997</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1996") echo 'selected="selected"'; ?> value="1996">1996</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1995") echo 'selected="selected"'; ?> value="1995">1995</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1994") echo 'selected="selected"'; ?> value="1994">1994</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1993") echo 'selected="selected"'; ?> value="1993">1993</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1992") echo 'selected="selected"'; ?> value="1992">1992</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1991") echo 'selected="selected"'; ?> value="1991">1991</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1990") echo 'selected="selected"'; ?> value="1990">1990</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1989") echo 'selected="selected"'; ?> value="1989">1989</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1988") echo 'selected="selected"'; ?> value="1988">1988</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1987") echo 'selected="selected"'; ?> value="1987">1987</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1986") echo 'selected="selected"'; ?> value="1986">1986</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1985") echo 'selected="selected"'; ?> value="1985">1985</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1984") echo 'selected="selected"'; ?> value="1984">1984</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1983") echo 'selected="selected"'; ?> value="1983">1983</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1982") echo 'selected="selected"'; ?> value="1982">1982</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1981") echo 'selected="selected"'; ?> value="1981">1981</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1980") echo 'selected="selected"'; ?> value="1980">1980</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1979") echo 'selected="selected"'; ?> value="1979">1979</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1978") echo 'selected="selected"'; ?> value="1978">1978</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1977") echo 'selected="selected"'; ?> value="1977">1977</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1976") echo 'selected="selected"'; ?> value="1976">1976</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1975") echo 'selected="selected"'; ?> value="1975">1975</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1974") echo 'selected="selected"'; ?> value="1974">1974</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1973") echo 'selected="selected"'; ?> value="1973">1973</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1972") echo 'selected="selected"'; ?> value="1972">1972</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1971") echo 'selected="selected"'; ?> value="1971">1971</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1970") echo 'selected="selected"'; ?> value="1970">1970</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1969") echo 'selected="selected"'; ?> value="1969">1969</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1968") echo 'selected="selected"'; ?> value="1968">1968</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1967") echo 'selected="selected"'; ?> value="1967">1967</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1966") echo 'selected="selected"'; ?> value="1966">1966</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1965") echo 'selected="selected"'; ?> value="1965">1965</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1964") echo 'selected="selected"'; ?> value="1964">1964</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1963") echo 'selected="selected"'; ?> value="1963">1963</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1962") echo 'selected="selected"'; ?> value="1962">1962</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1961") echo 'selected="selected"'; ?> value="1961">1961</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1960") echo 'selected="selected"'; ?> value="1960">1960</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1959") echo 'selected="selected"'; ?> value="1959">1959</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1958") echo 'selected="selected"'; ?> value="1958">1958</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1957") echo 'selected="selected"'; ?> value="1957">1957</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1956") echo 'selected="selected"'; ?> value="1956">1956</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1955") echo 'selected="selected"'; ?> value="1955">1955</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1954") echo 'selected="selected"'; ?> value="1954">1954</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1953") echo 'selected="selected"'; ?> value="1953">1953</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1952") echo 'selected="selected"'; ?> value="1952">1952</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1951") echo 'selected="selected"'; ?> value="1951">1951</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1950") echo 'selected="selected"'; ?> value="1950">1950</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1949") echo 'selected="selected"'; ?> value="1949">1949</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1948") echo 'selected="selected"'; ?> value="1948">1948</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1947") echo 'selected="selected"'; ?> value="1947">1947</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1946") echo 'selected="selected"'; ?> value="1946">1946</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1945") echo 'selected="selected"'; ?> value="1945">1945</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1944") echo 'selected="selected"'; ?> value="1944">1944</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1943") echo 'selected="selected"'; ?> value="1943">1943</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1942") echo 'selected="selected"'; ?> value="1942">1942</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1941") echo 'selected="selected"'; ?> value="1941">1941</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1940") echo 'selected="selected"'; ?> value="1940">1940</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1939") echo 'selected="selected"'; ?> value="1939">1939</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1938") echo 'selected="selected"'; ?> value="1938">1938</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1937") echo 'selected="selected"'; ?> value="1937">1937</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1936") echo 'selected="selected"'; ?> value="1936">1936</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1935") echo 'selected="selected"'; ?> value="1935">1935</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1934") echo 'selected="selected"'; ?> value="1934">1934</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1933") echo 'selected="selected"'; ?> value="1933">1933</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1932") echo 'selected="selected"'; ?> value="1932">1932</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1931") echo 'selected="selected"'; ?> value="1931">1931</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1930") echo 'selected="selected"'; ?> value="1930">1930</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1929") echo 'selected="selected"'; ?> value="1929">1929</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1928") echo 'selected="selected"'; ?> value="1928">1928</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1927") echo 'selected="selected"'; ?> value="1927">1927</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1926") echo 'selected="selected"'; ?> value="1926">1926</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1925") echo 'selected="selected"'; ?> value="1925">1925</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1924") echo 'selected="selected"'; ?> value="1924">1924</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1923") echo 'selected="selected"'; ?> value="1923">1923</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1922") echo 'selected="selected"'; ?> value="1922">1922</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1921") echo 'selected="selected"'; ?> value="1921">1921</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1920") echo 'selected="selected"'; ?> value="1920">1920</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1919") echo 'selected="selected"'; ?> value="1919">1919</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1918") echo 'selected="selected"'; ?> value="1918">1918</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1917") echo 'selected="selected"'; ?> value="1917">1917</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1916") echo 'selected="selected"'; ?> value="1916">1916</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1915") echo 'selected="selected"'; ?> value="1915">1915</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1914") echo 'selected="selected"'; ?> value="1914">1914</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1913") echo 'selected="selected"'; ?> value="1913">1913</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1912") echo 'selected="selected"'; ?> value="1912">1912</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1911") echo 'selected="selected"'; ?> value="1911">1911</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1910") echo 'selected="selected"'; ?> value="1910">1910</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1909") echo 'selected="selected"'; ?> value="1909">1909</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1908") echo 'selected="selected"'; ?> value="1908">1908</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1907") echo 'selected="selected"'; ?> value="1907">1907</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1906") echo 'selected="selected"'; ?> value="1906">1906</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1905") echo 'selected="selected"'; ?> value="1905">1905</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1904") echo 'selected="selected"'; ?> value="1904">1904</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1903") echo 'selected="selected"'; ?> value="1903">1903</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1902") echo 'selected="selected"'; ?> value="1902">1902</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1901") echo 'selected="selected"'; ?> value="1901">1901</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_birth_year'] == "1900") echo 'selected="selected"'; ?> value="1900">1900</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="user_grade" id="user_grade_error" class="custom-error-mssg" style="display: none;">Debes elegir una opción</label>
                                            <select style="font-size:15px; height:40px;" required id="user_grade" name="user_grade" class="form-control" onfocus="onFocusGrade();">
                                                <option <?php if (!$sr) echo 'selected="selected"'; ?> value="0">Elije tu nivel</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "primaria") echo 'selected="selected"'; ?> value="primaria">Primaria</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "secundaria") echo 'selected="selected"'; ?> value="secundaria">Secundaria</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "bachillerato") echo 'selected="selected"'; ?> value="bachillerato">Bachillerato</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "cfgm") echo 'selected="selected"'; ?> value="cfgm">Ciclo formativo grado medio</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "cfgs") echo 'selected="selected"'; ?> value="cfgs">Ciclo formativo grado superior</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "grado_universitario") echo 'selected="selected"'; ?> value="grado_universitario">Grado Universitario</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "master_universitario") echo 'selected="selected"'; ?> value="master_universitario">Master Universitario</option>
                                                <option <?php if ($sr && $cf['posted_form_data']['user_grade'] == "doctorado") echo 'selected="selected"'; ?> value="doctorado">Doctorado</option>
                                            </select>
                                        </div>
                                        
                                        <button type="submit" class="custom-button">Terminado</button>
                                        <p class="condiciones_mssg">Al hacer clic en el botón inferior aceptas las <a target="_blank" href="../term/">Condiciones de uso</a>.</p>

                                        <input type="hidden" id="abation_register_nonce" name="abation_register_nonce" value="<?php echo wp_create_nonce('abation-register-nonce'); ?>"/>

                                        <!--<p class="inicia_sesion_mssg">Ya tengo mi cuenta, <a href="../login/">inicia la sesión</a>.</p>-->
                                    </form>
                                    <?php unset($_SESSION['cf_returndata']); ?>
                                    <!--<form class="sign-up-intro" id="sign_up" method="POST">
                                        <div class="row">
                                        	<div class="col-md-12">
                                            	<div class="row">
                                        			<div class="col-md-6">
                                                        <p class="intro-name">
                                                            <span class="your-email">
                                                                <input type="text" autocomplete="off" name="email" id="email" value="" class="" placeholder="<?php _e("Email",ET_DOMAIN) ?>">
                                                                <i class="fa fa-envelope-o"></i>
                                                            </span>
                                                        </p>
                                                     </div>
                                                     <div class="col-md-6">
                                                     	<p class="intro-name">
                                                            <span class="your-name">
                                                                <input type="text" autocomplete="off" name="username" id="username" value="" class="" placeholder="<?php _e("User Name",ET_DOMAIN) ?>">
                                                                <i class="fa fa-user"></i>
                                                            </span>
                                                        </p>
                                                     </div>
                                                 </div>
                                            </div>
                                            <div class="col-md-12">
                                            	<div class="row">
                                        			<div class="col-md-6">
                                                         <p class="intro-password">
                                                            <span class="your-password">
                                                                <input type="password" autocomplete="off" name="password" id="password1" value="" class="" placeholder="<?php _e("Password",ET_DOMAIN) ?>">
                                                                <i class="fa fa-key"></i>
                                                            </span>
                                                        </p>
                                                     </div>
                                                     <div class="col-md-6">
                                                        <p class="intro-password">
                                                            <span class="your-password">
                                                                <input type="password" autocomplete="off" id="re_password" name="re_password" value="" class="" placeholder="<?php _e("Repeat Password",ET_DOMAIN) ?>">
                                                                <i class="fa fa-key"></i>
                                                            </span>
                                                        </p>
                                                     </div>
                                                </div>
                                            </div>-->
                                            <!--<div class="col-md-12">
                                            <?php ae_gg_recaptcha(); ?>
                                            </div>--><!-- END GG CAPTCHA -->
                                            <!--<div class="col-md-12">
                                            	<p class="terms-intro">
                                                     <?php _e("By clicking \"Sign Up\" you indicate that you have read and agree to the",ET_DOMAIN) ?> <a target="_blank" href="<?php echo et_get_page_link('term') ?>"><?php _e("Terms of Service.",ET_DOMAIN) ?></a>
                                                </p>
                                            </div>
                                            <div class="col-md-12">
                                            	<p class="btn-submit-intro">
                                                    <span class="your-submit mobile-device">
                                                        <input type="submit" name="" value="<?php _e("Sign up",ET_DOMAIN) ?>" class="btn-submit" />
                                                    </span>
                                                </p>
                                            </div>
                                        </div>
                                    </form>-->
                                </div>
                                <?php } ?>

                                <?php if($disabled_register == 1 || $disabled_register == "user" || $disabled_register == "all" || $disabled_register == "blog" ){ ?>
                                    <div class="sign-in-social">
                                        <ul class="social-icon clearfix">
                                            <!-- google plus login -->
                                            <?php if(ae_get_option('gplus_login', false)){?>
                                                <li class="gp"><a id="signinButton" href="#" class="sc-icon color-google gplus_login_btn" ><i class="fa fa-google-plus-square"></i></a></li>
                                            <?php } ?>
                                            <!-- twitter plus login -->
                                            <?php if(ae_get_option('twitter_login', false)){?>
                                                <li class="tw"><a href="<?php echo add_query_arg('action', 'twitterauth', home_url()) ?>" class="sc-icon color-twitter" ><i class="fa fa-twitter-square"></i></a></li>
                                            <?php } ?>
                                            <!-- facebook plus login -->
                                            <?php if(ae_get_option('facebook_login', false)){?>
                                                <li class="fb"><a href="#" id="facebook_auth_btn" class="sc-icon color-facebook facebook_auth_btn" ><i class="fa fa-facebook-square"></i></a></li>
                                            <?php } ?>
                                            <?php if(ae_get_option('linkedin_login', false)){?>
                                                <li class="fb"><a href="#" id="linked_login_id" class="sc-icon color-facebook linkedin_login" ><i class="fa fa-linkedin-square"></i></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                <?php } ?>
                            </div>
                    	</div>
                        <div class="clearfix" style="height:50px;"></div>



                    </div>
                    <!-- END FORM -->
                 </div>
                 <!-- END CONTENT INTRO
                 <div class="clearfix"></div>
                 <div class="footer-intro">
                 	<div class="col-md-12">
                    	<ul class="list-menu-footer">
                            <?php
                                /*if(has_nav_menu('et_header')){
                                    wp_nav_menu(array(
                                            'theme_location' => 'et_header',
                                            'items_wrap' => '%3$s',
                                            'container' => ''
                                        ));
                                }*/
                            ?>
                        </ul>
                    </div>
                 </div>
                 -->
            </div>
        </div>
        <div class="clearfix" style="height:120px;"></div>
		<script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/libs/selectivizr-min.js"></script>
        <script>
			jQuery(document).ready(function($) {
                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)){
                    $(".adject").textrotator({
                        animation: "dissolve",
                        separator: "|",
                        speed: 2000
                    });
                } else {
                    $(".adject").textrotator({
                        animation: "flipUp",
                        separator: "|",
                        speed: 2000
                    });
                }

				$(".to_register").click(function(){
					$(".group-btn-intro").find('.to_register').removeClass('active');
					$(this).addClass('active');
				});
				$(".to_login_active").click(function(){
					$('.form-signup-wrapper').css({'min-height':'210px'});
				});
				$(".to_register_active").click(function(){
					$('.form-signup-wrapper').css({'min-height':'300px'});
				});
			});
		</script>
        <!-- Style Intro Background -->
        <?php
            $bg_images = ae_get_option('intro_background');
            $bg_images = wp_get_attachment_image_src($bg_images['attach_id'],'full');
            //print_r($bg_images);
            if($bg_images){
        ?>
        <style type="text/css">
            .intro-wrapper {
                background: url(<?php echo $bg_images[0]; ?>) no-repeat;
                background-size: cover;
                background-attachment: fixed;
                background-position: top center;
            }
        </style>
        <?php } ?>
        <!-- Style Intro Background -->
    <?php wp_footer(); ?>
	</body><!-- END BODY -->
</html>