<?php
define( 'MOBILE_PATH', dirname(__FILE__) );
/**
*
* TEMPLATE FILTER QUESTIONS FOR MOBILE
* @author ThaiNT
* @since 1.0
*
**/
function qa_mobile_submit_questions_form(){
	$privi  =   qa_get_privileges();
	?>

	<!-- Modal upload image -->
	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" id="myModalLabel">Subir Imagen</h4>
	      </div>
	      <div class="modal-body">
	        <form action="uploadck.php" method="post" enctype="multipart/form-data" id="MyUploadForm">
	        	<div class="form-group">
	        		<label for="sortpicture" id="error_label_file" style="color:red; display:none">No has añadido ninguna imagen</label>
	        		<input id="sortpicture" class="form-control" type="file" name="sortpic" onfocus="onFocusInput()" />
	        	</div>

	        	<div class="form-group">
	        		<button type="submit" class="btn btn-success" id="upload" style="width:80px"><i id="bt_loading" class="fa fa-spinner fa-spin" style="display:none"></i><span id="bt_text">Subir</span></button>
	        	</div>
			</form>
	      </div>
	    </div>
	  </div>
	</div>
	<!-- END Modal upload image -->

	<!-- Upload image script -->
	<script>

		var flagFieldType = "";

		$(document).ready(function() {
		    $("#myBtn").click(function() {
		      $("#myModal").modal("toggle");
		      	flagFieldType = "question";
		    });

		    $("#myBtnAnswer").click(function() {
		      $("#myModal").modal("toggle");
		      	flagFieldType = "answer";
		    });

		    $("#myBtnComment").click(function() {
		      $("#myModal").modal("toggle");
		      	flagFieldType = "comment";
		    });
		});

		$('#MyUploadForm').submit(function(e) {

			e.preventDefault();
			startAnimation();

			if (!$("#sortpicture").val().length) {
				$("#error_label_file").show();
				stopAnimation();
			}
			else {
				var ext = $('#sortpicture').val().split('.').pop().toLowerCase();

				if ($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
					$("#error_label_file").text("Formatos permitidos: gif, png, jpeg y jpg");
	    			$("#error_label_file").show();
	    			stopAnimation();
	    		}
				else {
					var file_data = $('#sortpicture').prop('files')[0];   
				    var form_data = new FormData();                  
				    form_data.append('file', file_data);
                          
				    $.ajax({
				        url: 'http://academia.abation.es/uploadck.php', // point to server-side PHP script 
				        dataType: 'text',  // what to expect back from the PHP script, if anything
				        cache: false,
				        contentType: false,
				        processData: false,
				        data: form_data,                         
				        type: 'post',
				        success: function(php_script_response){

				        	if(php_script_response == 0) {                    
			        			$("#error_label_file").text("Tipo o peso incorrecto").show();
						    }
						    else if (php_script_response == -1) {
						    	$("#error_label_file").text("Ha habido algún error").show();
						    }
						    else if (php_script_response == -2) {
						    	$("#error_label_file").text("Error interno del server").show();
						    }
						    else if (php_script_response == -3) {
						    	$("#error_label_file").text("Error: no se ha podido guardar").show();
						    }
						    else {
				          		//var text = $('#post_content').val();
				          		//$('#post_content').val(text + php_script_response);//Update textarea content

				          		var textArea = $("textarea[data-type='" + flagFieldType + "']");
				          		var text = textArea.val();
				          		textArea.val(text + php_script_response);//Update textarea content	

						      	var control = $("#sortpicture");
								control.replaceWith( control = control.clone( true ) );//Initialize input type file

								$("#error_label_file").hide();
								$("#myModal").modal("hide");
						    }

						    stopAnimation();
			       		},
			       		error: function(php_script_response) { 
			       			$("#error_label_file").text("Server error, please try again.").show();
			       			stopAnimation();
			       		}
				 	});
				}
			}
		});

		function onFocusInput() {
			$("#error_label_file").hide();
			$("#error_label_file").text("No has añadido ninguna imagen");
		}

		function startAnimation() {
			$("#upload").prop('disabled',true);
			$("#bt_loading").show();
			$("#bt_text").hide();
		}

		function stopAnimation() {
			$("#upload").prop('disabled',false);
			$("#bt_loading").hide();
			$("#bt_text").show();
		}

	</script>
	<!-- END Upload image script -->

	<section class="post-question-form-wrapper">
		<form id="submit_question" action="">
		<input type="hidden" id="qa_nonce" name="qa_nonce" value="<?php echo wp_create_nonce( 'insert_question' ); ?>">
		<input id="add_tag_text" type="hidden" value="<?php printf(__("You must have %d points to add tag. Current, you have to select existed tags.", ET_DOMAIN), $privi->create_tag  ); ?>" />
		<div class="container">
	        <div class="row">
	        	<div class="col-md-12">
	                	<div class="form-post">
	                		<input type="text" name="post_title" id="question_title" placeholder="<?php _e("Your Question", ET_DOMAIN) ?>">
	                    </div>
	                    <div class="form-post">
	                        <div class="select-categories-wrapper">
	                            <div class="select-categories">
	                                <select class="select-grey-bg" id="" name="question_category">
										<option value=""><?php _e("Select Category",ET_DOMAIN) ?></option>
										<?php
											$terms = get_terms( 'question_category', array('hide_empty' => 0) );
											foreach ($terms as $term) {
										?>
										<option value="<?php echo $term->slug ?>"><?php echo $term->name ?></option>
										<?php
											}
										?>
	                                </select>
	                            </div>
	                        </div>
	                    </div>
	                    <!-- Upload image -->
			            <div class="form-post">
							<button type="button" class="btn btn-info btn-md" id="myBtn">Subir Imagen</button>
					    </div>
	                    <!-- END upload image -->
	                    <div class="form-post">
	                    	<textarea data-type="question" name="post_content" id="" cols="30" rows="10" placeholder="<?php _e("Your Description", ET_DOMAIN) ?>"></textarea>
	                    </div>
	                    <div class="form-post">
	                    	<input  data-provide="typeahead" type="text" name="" id="question_tags" placeholder="<?php _e('Tag(max 5 tags)',ET_DOMAIN) ?>" />
	                    </div>
	                    <ul class="post-question-tags" id="tag_list"></ul>
	            </div>
	        </div>
	    </div>
	    <div class="group-btn-post">
	    	<div class="container">
	            <div class="row">
	                <div class="col-xs-5"><span class="text"><?php _e("Ask a Question", ET_DOMAIN) ?></span></div>
	                <div class="col-xs-7 text-right">
	                	<button type="submit" class="submit-post-question"><?php _e("Submit", ET_DOMAIN) ?></button>
	                    <a href="javascript:void(0)" class="cancel-post-question"><?php _e("Cancel", ET_DOMAIN) ?></a>
	                </div>
	            </div>
	        </div>
	    </div>
		</form>
	</section>
	<?php
}
/**
*
* TEMPLATE FILTER QUESTIONS FOR MOBILE
* @param array
* @author ThaiNT
* @since 1.0
*
**/
function qa_mobile_filter_search_questions(){
	$current_url = "http".(isset($_SERVER['HTTPS']) ? 's' : '')."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	?>
	<div class="container">
	    <div class="row">
	    	<div class="col-md-12">
	        	<ul class="menu-middle-bar">
	                <li class="<?php echo !isset($_GET['sort']) ? 'active' : ''; ?>">
	                    <a href="<?php echo remove_query_arg( array('sort') ,$current_url); ?>">
	                    	<?php _e("Latest",ET_DOMAIN) ?>
	                    </a>
	                </li>
	                <li class="<?php echo isset($_GET['sort']) && $_GET['sort'] == 'vote' ? 'active' : ''; ?>" >
	                    <a href="<?php echo esc_url(add_query_arg(array('sort' => 'vote'))); ?>">
	                    	<?php _e("Votes",ET_DOMAIN) ?>
	                    </a>
	                </li>
	                <li class="<?php echo isset($_GET['sort']) && $_GET['sort'] == 'unanswer' ? 'active' : ''; ?>">
	                    <a href="<?php echo esc_url(add_query_arg(array('sort' => 'unanswer'))); ?>">
	                    	<?php _e("Unanswered",ET_DOMAIN) ?>
	                    </a>
	                </li>
	            </ul>
	        </div>
		</div>
	</div>
	<div class="form-search-wrapper" <?php if(is_page_template( 'page-search.php' )){ ?>style="display:block;"<?php } ?>>
	    <?php
	        $keyword = get_query_var( 'keyword' ) ? urldecode(get_query_var( 'keyword' )) : '';
	        $keyword = str_replace('+', ' ', $keyword);
	    ?>
		<form id="form-search" method="POST" action="<?php echo home_url(); ?>">
	    	<a href="javascript:void(0)" class="clear-text-search"><i class="fa fa-times-circle"></i></a>
	        <a href="javascript:void(0)" class="close-form-search"><?php _e("Cancel",ET_DOMAIN) ?></a>
	        <input type="text" class="form-input-search" autocomplete="off" name="keyword" value="<?php echo esc_attr($keyword) ?>" placeholder="<?php _e("Enter Keywords",ET_DOMAIN) ?>" />
	    </form>
	</div>
	<?php
}
/**
*
* TEMPLATE FORM COMMENTS FOR MOBILE
* @param array $comments
* @author ThaiNT
* @since 1.0
*
**/
function qa_mobile_comment_form( $post, $type = 'question' ){
	global $current_user;
?>
<form class="form-post-answers create-comment collapse">
    <input type="hidden" name="qa_nonce"        value="<?php echo wp_create_nonce( 'insert_comment' );?>" />
    <input type="hidden" name="comment_post_ID" value="<?php echo $post->ID ?>" />
    <input type="hidden" name="comment_type"    value="<?php echo $type ?>" />
    <input type="hidden" name="user_id"         value="<?php echo $current_user->ID ?>" />
    <!-- Upload image button -->
    <button type="button" style="margin-bottom:10px" class="btn btn-info btn-md" id="myBtnComment">Subir Imagen</button>
    <!-- END Upload image button -->
	<textarea data-type="comment" name="post_content" id="post_content" rows="4" placeholder="<?php _e("Type your comment", ET_DOMAIN)?> "></textarea>
	<input type="submit" class="btn-submit" name="submit" id="" value="<?php _e("Add comment", ET_DOMAIN)?>">
	<a href="javascript:void(0)" class="close-form-post-answers"><?php _e("Cancel", ET_DOMAIN)?></a>
</form>
<?php
}
/**
*
* TEMPLATE LOOP FOR MOBILE COMMENTS
* @param array $comments
* @author ThaiNT
* @since 1.0
*
**/
function qa_mobile_comments_loop($child){
	global $qa_comment;
	$qa_comment = QA_Comments::convert($child);
	get_template_part( 'mobile/template/item' , 'comment' );
}

/**
*
* JS TEMPLATE ANSWER
* @param array $comments
* @author ThaiNT
* @since 1.0
*
**/
function qa_mobile_answer_template(){
	// get template-js/item-answer.php
	get_template_part( 'mobile/template-js/item', 'answer' );
}
function qa_mobile_comment_template(){
	// get template-js/item-answer.php
	get_template_part( 'mobile/template-js/item', 'comment' );
}
/**
 * New Script in Mobile Footer
 */
if ( et_load_mobile()  ){
add_action( 'wp_footer', 'scripts_in_footer_mobile', 100);
}
function scripts_in_footer_mobile(){
?>
<script type="text/javascript" id="frontend_scripts">
	(function ($) {
		$(document).ready(function(){
			if(typeof QAEngine.Views.MobileFront != 'undefined') {
				QAEngine.MobileApp = new QAEngine.Views.MobileFront();
			}
			if(typeof QAEngine.Views.MobileSingleQuestion != 'undefined') {
				QAEngine.MobileSingleQuestion = new QAEngine.Views.MobileSingleQuestion();
			}
			if(typeof QAEngine.Views.ChangeProfileModal != 'undefined') {
				QAEngine.MobileApp = new QAEngine.Views.ChangeProfileModal();
			}
		});
	})(jQuery);
</script>
<?php
}
/**
 * Enqueue Styles & Scripts
 */
if ( et_load_mobile()  ){
add_action( 'wp_enqueue_scripts', 'qa_mobile_scripts_styles' );
}
function qa_mobile_scripts_styles(){
	/* ==== PRINT SCRIPTS ==== */
	wp_enqueue_script('mobile-front', 	TEMPLATEURL . '/mobile/js/front.js', array('jquery', 'underscore', 'backbone', 'site-functions'));
	wp_enqueue_script('mouseweel', 		TEMPLATEURL . '/mobile/js/jquery.mouseweel.js', array('jquery'));
	wp_enqueue_script('mobile-script', 	TEMPLATEURL . '/mobile/js/script-mobile.js', array('jquery'));
	wp_enqueue_script('mobile-script', 	TEMPLATEURL . '/js/libs/adjector.js', array('jquery'));

	//localize scripts
	wp_localize_script( 'mobile-front', 'qa_front', qa_static_texts() );

	if(is_singular( 'question' )){
		wp_enqueue_script('mobile-single-question', 	TEMPLATEURL . '/mobile/js/single-question.js', array('jquery', 'underscore', 'backbone', 'site-functions','mobile-front'));
	}
	/* ==== PRINT STYLES ==== */
	wp_enqueue_style( 'mobile-style', 	TEMPLATEURL . '/mobile/css/main.css', array('bootstrap') );
}