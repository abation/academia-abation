<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<!--[if lt IE 7]> <html class="ie ie6 oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="ie ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="ie ie8 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]> <html class="ie ie9 newest" lang="en"> <![endif]-->
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta charset="utf-8">
		<link rel="canonical" href="http://www.academia.abation.es/" />
		<link  href="https://plus.google.com/105716872685734442677" rel="publisher"/>
		<meta name="keywords" content="Como se hace, necesito ayuda, pregunta, respuesta, problemas, 	 		aprobar mates, tutoriales, bachillerato, secundaria, eso" /> 
		<meta name="description" content="Aprobar bachillerato y ESO, secundaria es fácil. Las respuestas a tus preguntas y los tutoriales que necesitas."/>
		<meta property="og:locale" content="es_ES" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="Preguntas y respuestas - Tutoriales | Academia Abation" />
		<meta property="og:description" content="Preguntas, respuestas y tutoriales. Como se hace. Aprobar ESO y Bachillerato en matemáticas, mates, física, química, lengua e Inglés." />
		<meta property="og:url" content="http://www.academia.abation.es/" />
		<meta property="og:site_name" content="Academia Abation" />
		<meta property="article:publisher" content="https://www.facebook.com/abationprojects" />
		<meta property="og:image" content="http://www.academia.abation.es/wp-content/uploads/2015/09/Fondo-Youtube.png" />
		<meta name="twitter:description" content="Preguntas, respuestas y tutoriales. Como se hace. Aprobar ESO y Bachillerato en matemáticas, mates, física, química, lengua e Inglés."/>
		<meta name="twitter:title" content="Preguntas y respuestas | Tutoriales | Academia Abation"/>
		<meta name="twitter:site" content="@AcademiaAbation"/>
		<meta name="twitter:domain" content="AcademiaAbation"/>
		<meta name="http://www.academia.abation.es/wp-content/uploads/2015/09/Fondo-Youtube.png"/>
	

	<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-61340191-2', 'auto');
  ga('send', 'pageview');



</script>

        <?php
			$options = AE_Options::get_instance();
			$favicon = $options->mobile_icon['thumbnail'][0];
        ?>
        <title><?php wp_title( '|', true, 'right' ); ?></title>
		<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
		<link rel="shortcut icon" href="<?php echo $favicon ?>"/>
		<link href='//fonts.googleapis.com/css?family=Lato:400,700&subset=latin,cyrillic,cyrillic-ext,vietnamese,latin-ext' rel='stylesheet' type='text/css'>
		<script type="text/javascript" src="<?php echo TEMPLATEURL ?>/js/libs/selectivizr-min.js"></script>

		<!-- jquery scripts 11112015-->
		<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
		<script src="/my_analytics_tracking.js"></script>
		<!-- END my jquery scripts -->

		<script>
			$(document).ready(function() {
				$('#custom-login-button').hover(
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light');
				    	$(this).addClass('mint-button-secondary--light-inverse'); 
				    },
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light-inverse'); 
				    	$(this).addClass('mint-button-secondary--light'); 
				    }
				)

				$('#custom-register-button').hover(
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light-inverse'); 
				    	$(this).addClass('mint-button-secondary--light'); 
				    },
				    function(){ 
				    	$(this).removeClass('mint-button-secondary--light');
				    	$(this).addClass('mint-button-secondary--light-inverse');  
				    }
				)
			});
		</script>

		<style>
			.mint-hero__button {
				margin-right: 12px;
			}

			.mint-button-secondary {
				text-decoration: none;
			    display: inline-block;
			    /*overflow: hidden;*/
			    line-height: 1.5rem;
			    letter-spacing: .03rem;
			    cursor: pointer;
			    border-radius: 2rem;
			    height: 3rem;
			    padding: 0 .99975rem;
			    border-color: #6ed6a0;
			    color: #6ed6a0!important;
			    fill: #6ed6a0;
			    border-width: 2px;
			    border-style: solid;
			    background-color: transparent;
			    transition: background-color .3s ease-out,color .3s ease-out,fill .3s ease-out;

			    text-overflow: ellipsis;
			    min-height: 1.5rem;
			    margin: 0;
			    position: relative;
			    font-size: .95rem;

			    text-transform: uppercase;
	    		font-weight: 700;
			}

			.mint-button-secondary--light-inverse {
			    border-color: #fff;
			    color: black!important;
			    fill: #000;
			    background-color: #fff;
			}

			.mint-button-secondary--light {
			    border-color: #fff;
			    color: white!important;
			    fill: #fff;
			    background-color: transparent;
			}

			.mint-button-secondary__hole {
			    display: -webkit-flex;
			    display: -ms-flexbox;
			    display: flex;
			    -webkit-flex-wrap: wrap;
			    -ms-flex-wrap: wrap;
			    flex-wrap: wrap;
			    -webkit-align-items: center;
			    -ms-flex-align: center;
			    align-items: center;
			    height: 100%;
			    -webkit-justify-content: center;
			    -ms-flex-pack: center;
			    justify-content: center;
			}
		</style>

		<?php
		    //loads comment reply JS on single posts and pages
		    if ( is_single()) wp_enqueue_script( 'comment-reply' );
	    ?>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
	    <?php wp_head(); ?>
	</head>
	<body <?php echo body_class('cbp-spmenu-push') ?>>
		<div class="sampleClass"></div>
    	<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left" id="cbp-spmenu-s1">
			<?php get_sidebar( 'left-tablet' ); ?>
		</nav>
		<nav class="cbp-spmenu cbp-spmenu-vertical cbp-spmenu-right" id="cbp-spmenu-s2">
			<?php get_sidebar( 'right-tablet' ); ?>
		</nav>

		<nav class="cbp-spmenu cbp-spmenu-horizontal cbp-spmenu-top" id="cbp-spmenu-s3">
			<ul class="menu-res">
				<?php
					if(has_nav_menu('et_header')){
						wp_nav_menu(array(
							'theme_location' => 'et_header',
							'items_wrap'     => '%3$s',
							'container'      => ''
						));
					}
				?>
			</ul>
		</nav>


		<?php
			global $current_user;
			$site_logo = ae_get_option('site_logo');
		?>
		<div class="container-fluid">
			<div class="row">
				<header id="header">
					<div class="col-md-2 col-xs-2" id="logo">
						<a href="<?php echo home_url(); ?>">
							<?php if(!empty($site_logo)){ ?>
							<img src="<?php echo $site_logo['large'][0] ?>">
							<?php } else { ?>
							<img src="<?php echo TEMPLATEURL ?>/img/logo.png">
							<?php } ?>
						</a>
					</div><!-- logo -->
					<div id="menu_qa" class="col-md-8 col-xs-8">
						<?php if(has_nav_menu('et_header')){ ?>
                        <div class="header-menu">
                            <ul>
                                <?php
                                    wp_nav_menu(array(
										'theme_location' => 'et_header',
										'items_wrap'     => '%3$s',
										'container'      => ''
                                    ));
                                ?>
                            </ul>
                        </div><!-- menu -->
                        <?php } ?>
                        <div class="header-search-wrapper">
                        	<section class="buttonset">
                                <button id="showLeftPush"><i class="fa fa-question"></i></button>
                                <button id="showRightPush"><i class="fa fa-bar-chart-o"></i></button>

                                <!-- <button id="showLeft"><i class="fa fa-question"></i></button>
								<button id="showRight"><i class="fa fa-bar-chart-o"></i></button> -->
								<button id="showTop"><i class="fa fa-list"></i></button>
                            </section>
                        	<?php
                        		$keyword = get_query_var( 'keyword' ) ? get_query_var( 'keyword' ) : '';
                        		$keyword = str_replace('+', ' ', $keyword);
                        	?>
                            <form id="header_search" method="GET" action="<?php echo home_url(); ?>" class="disable-mobile-search">
                                <input type="text" name="keyword" value="<?php echo esc_attr(urldecode($keyword)) ?>" placeholder="<?php _e("Enter Keywords",ET_DOMAIN) ?>" autocomplete="off" />
                                <i class="fa fa-search"></i>
                                <div id="search_preview" class="search-preview empty"></div>
                            </form>
                        </div><!-- search -->
					</div>
					<div id="login_qa" class="col-md-2 col-xs-2 btn-group <?php echo is_user_logged_in() ? 'header-avatar ' : ''; ?>">
						<?php
							if(is_user_logged_in()) {
								global $current_user;
						?>
                    	<span class="expand dropdown-toggle" type="span" data-toggle="dropdown">
							<a href="javascript:void(0)" class="dropdown-account " >
                                <span class="avatar"><?php echo et_get_avatar( $current_user->ID, 30 ); ?></span>
                                <span class="display_name"><?php echo $current_user->display_name ?></span>
                                <span class="icon-down"><i class="fa fa-chevron-circle-down"></i></span>
                        	</a>
						</span>
						<ul class="dropdown-menu dropdown-profile">
							<li>
								<a href="<?php echo get_author_posts_url($current_user->ID); ?>">
									<i class="fa fa-user"></i> <?php _e("User Profile",ET_DOMAIN) ?>
								</a>
							</li>
							<li>
								<a href="javascript:void(0)" class="open-edit-profile edit_profile">
									<i class="fa fa-cog"></i> <?php _e("User Settings",ET_DOMAIN) ?>
								</a>
							</li>

							<li>
								<a href="<?php echo wp_logout_url(home_url()); ?>">
									<i class="fa fa-power-off"></i> <?php _e("Log out",ET_DOMAIN) ?>
								</a>
							</li>
						</ul>

						<?php } else {?>

						<!--<a class="login-url" href="javascript:void(0)" data-toggle="modal">
							<?php _e("Login or Register",ET_DOMAIN)?>
						</a>-->
						<div style="line-height: 80px; display: block; text-align: center;">
				    		<!--<a id="login-link" href="/login" class="login" data-track="click_log_in_link" data-kiss-event="click_login_navbar">Iniciar sesión</a>
				        	<span style="color:white">|</span>
				        	<a id="register-link" href="/register" class="sign-up" data-track="click_register_link" data-kiss-event="click_register_navbar">Registrarse</a>-->
				        	<!--<a id="custom-login-button" class="mint-button-secondary mint-button-secondary--light mint-hero__button" data-track="click_log_in_link" href="/login">
						        <div class="mint-button-secondary__hole">Entrar</div>
						    </a>-->

						    <a id="custom-register-button" class="mint-button-secondary mint-button-secondary--light-inverse mint-hero__button" data-track="click_register_link" href="/login">
						        <div class="mint-button-secondary__hole">Entrar / Registro</div>
						    </a>
					    </div>
						<?php } ?>
					</div><!-- avatar -->
				</header><!-- END HEADER -->
				<div class="col-md-12 col-xs-12" id="header_sidebar">
					<?php if(is_active_sidebar( 'qa-header-sidebar' )) dynamic_sidebar( 'qa-header-sidebar' ); ?>
				</div>