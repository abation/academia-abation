<?php
define("ET_UPDATE_PATH", "http://www.enginethemes.com/forums/?do=product-update");
define("ET_VERSION", '1.5.4');

if(!defined('ET_URL'))
	define('ET_URL', 'http://www.enginethemes.com/');

if(!defined('ET_CONTENT_DIR'))
	define('ET_CONTENT_DIR', WP_CONTENT_DIR.'/et-content/');

define( 'TEMPLATEURL', get_template_directory_uri() );
define( 'THEME_NAME' , 'qaengine');
define( 'ET_DOMAIN'  , 'enginetheme');

if(!defined('THEME_CONTENT_DIR ')) 	define('THEME_CONTENT_DIR', WP_CONTENT_DIR . '/et-content' . '/qaengine' );
if(!defined('THEME_CONTENT_URL'))	define('THEME_CONTENT_URL', content_url() . '/et-content' . '/qaengine' );

// theme language path
if(!defined('THEME_LANGUAGE_PATH') ) define('THEME_LANGUAGE_PATH', THEME_CONTENT_DIR.'/lang/');

if(!defined('ET_LANGUAGE_PATH') )
 define('ET_LANGUAGE_PATH', THEME_CONTENT_DIR . '/lang');

if(!defined('ET_CSS_PATH') )
 define('ET_CSS_PATH', THEME_CONTENT_DIR . '/css');

require_once TEMPLATEPATH.'/includes/index.php';
require_once TEMPLATEPATH.'/mobile/functions.php';

try {
	if ( is_admin() ){
		new QA_Admin();
	} else {
		new QA_Front();
	}
} catch (Exception $e) {
	echo $e->getMessage();
}
add_filter('gettext', 'et_translate_text' , 99, 3);
function et_translate_text ( $translated_text, $text, $domain ) {
	$translation = array (
		'YOU MUST <a class="authenticate" href="%s">LOGIN</a> TO SUBMIT A REVIEW' => 'YOU HAVE TO <a class="authenticate" href="%s">SIGNIN</a> TO CREATE A REVIEW',
	);
	if( isset( $translation[$text] ) ) {
		return $translation[$text];
	}
	return $translated_text;
}
//remove_action('admin_init', 'block_dashboard');

// Redirect to page Intro when register by the "Back door" (/wp-login.php?action=register)
if(is_multisite()){
	add_filter( 'wp_signup_location', 'my_custom_signup_location' );
	function my_custom_signup_location( $url ) {
	    return et_get_page_link('intro');
	}
}else{
	add_filter( 'registration_redirect', 'wpse_46848_hijack_the_back' );
	function wpse_46848_hijack_the_back( $redirect_to ) {
	    wp_redirect( et_get_page_link('intro') );
	    return exit();
	}
}

/***************************** ********************* *******************************/
/*****************************                       *******************************/
/*****************************    MY FUCKING CODE!   *******************************/
/*****************************                       *******************************/
/***************************** ********************* *******************************/

/**
 * Custom Login a new user.
 */

function abation_custom_login_user() {
  if(isset($_GET['do']) && $_GET['do'] == 'login') {

    $errors = array();
    $user_email = null;

    if(isset($_POST['user_email']) && wp_verify_nonce($_POST['abation_login_nonce'], 'abation-login-nonce')) {

      // this returns the user ID and other info from the user name
      $user_email = $_POST["user_email"];
      $user = null;

      if (is_email($user_email)) {
        //get user by email
        $user = get_user_by('email',$user_email);
      }
      /*else { //user_login is a nickname
        //user = get_userdatabylogin($user_login);
        $user = get_user_by('login',$user_login);
      }*/
      
      if(!$user) {
        $errors[] = 'Email incorrecto';
      }
      elseif(!isset($_POST['user_password']) || $_POST['user_password'] == '') {
        // if no password was entered
        $errors[] = 'No has introducido ninguna contraseña';
      }
      elseif(!wp_check_password($_POST['user_password'], $user->user_pass, $user->ID)) {
        // if the password is incorrect for the specified user
        $errors[] = 'Email o contraseña incorrectos';
      }
   
      // only log the user in if there are no errors
      if(empty($errors)) {
        //echo "entra";
        wp_setcookie($user->user_login, $_POST['user_password'], true);
        wp_set_current_user($user->ID, $user->user_login);  
        do_action('wp_login', $user->user_login);
   
        wp_redirect(home_url()); 
        exit; //or die(). This is for when you use "header" to redirect the page, otherwise the rest of the script continues running
      }
    }
    else {
      //Nonce does not match
      $errors[] = 'Por favor, inténtalo de nuevo.';
    }

    //print_r ($errors);

    //what we need to return back to our form
      $returndata = array(
          'posted_form_data' => array(
              'user_email' => $user_email
          ),
          'errors' => $errors
      );

      session_start();
      $_SESSION['cf_returndata'] = $returndata;
  }
}

add_action('template_redirect', 'abation_custom_login_user');

/**
 * Custom Register a new user.
 */

function abation_custom_register_user() {
  if(isset($_GET['do']) && $_GET['do'] == 'register') {

    $errors = array();

    if (isset( $_POST["user_nickname"] ) && wp_verify_nonce($_POST['abation_register_nonce'], 'abation-register-nonce')) {
      $user_login = $_POST["user_nickname"];  
      $user_email = $_POST["user_email"];
      $user_pass = $_POST["user_password"];
      $user_birth_year = $_POST["user_birth_year"];
      $user_sex = $_POST["user_sex"];
      $user_grade = $_POST["user_grade"];

      // this is required for username checks
      //require_once(ABSPATH . WPINC . '/registration.php');

      if(username_exists($user_login)) {
        // username already registered
        $errors[] = "El nombre de usuario introducido ya está en uso";
      }
      elseif(!validate_username($user_login)) {
        // invalid username
        $errors[] = "Nombre de usuario inválido";
      }
      elseif($user_login == '') {
        $errors[] = "El nombre de usuario no puede estar vacío";
      }
      elseif(!preg_match('/^.{4,}$/', $user_login)) {
        $errors[] = "El nombre de usuario debe tener al menos 4 caracteres";
      }

      if(!is_email($user_email)) {
        // invalid email
        $errors[] = "Email inválido";
      }
      elseif(email_exists($user_email)) {
        // Email address already registered
        $errors[] = "El email introducido ya está en uso";
      }

      if($user_pass == '' || !preg_match('/^.{6,}$/', $user_pass)) {
        // Empty password
        $errors[] = "La contraseña debe tener al menos 6 caracteres";
      }

      if($user_birth_year == '') {
        // Empty birth year
        $errors[] = "No has seleccionado tu año de nacimiento";
      }

      if($user_sex == '') {
        // Empty sex
        $errors[] = "No has seleccionado tu sexo";
      }

      if($user_grade == '') {
        // Empty grade
        $errors[] = "No has seleccionado tu nivel académico";
      }

      // only create the user in if there are no errors
      if(empty($errors)) {
   
        $new_user_id = wp_insert_user(array(
            'user_login'    => $user_login,
            'user_pass'     => $user_pass,
            'user_email'    => $user_email,
            'user_registered' => date('Y-m-d H:i:s'),
            'role'        => 'author'
          )
        );

        //User registered OK!
        if($new_user_id) {
          // add user meta data
          add_user_meta($new_user_id, "_sex", $user_sex);
          add_user_meta($new_user_id, "_birth_year", $user_birth_year);
          add_user_meta($new_user_id, "_grade", $user_grade);
	        //$ad_tracking = "home";
	        //add_user_meta($new_user_id, "_ad_tracking", $ad_tracking);
          add_user_meta($new_user_id, "_user_ip", $_SERVER['REMOTE_ADDR']);
          add_user_meta($new_user_id, "_user_device", getUserDevice());
          //add_user_meta($new_user_id, "_user_device_model", getUserDeviceModel());
          add_user_meta($new_user_id, "_user_browser", getUserBrowser());
          add_user_meta($new_user_id, "_user_os", getUserOS());
          add_user_meta($new_user_id, "_user_agent", $_SERVER['HTTP_USER_AGENT']);

          // send an email to the admin alerting them of the registration
          wp_new_user_notification($new_user_id);

	       // send an email to the new registered user
          sendEmailToNewUser($new_user_id);
   
          // log the new user in
          wp_setcookie($user_login, $user_pass, true);
          wp_set_current_user($new_user_id, $user_login); 
          do_action('wp_login', $user_login);
   
          // send the newly created user to the home page after logging them in
          wp_redirect(home_url()); 
          exit; //or die(). This is for when you use "header" to redirect the page, otherwise the rest of the script continues running
        }
        else {
          //Cannot insert user in ddbb
          $errors[] = "Parece que hemos hecho algo mal :( Por favor, inténtalo de nuevo";
        }
      }
    }
    else {
      //Nonce does not match
      $errors[] = "PParece que hemos hecho algo mal :( Por favor, inténtalo de nuevo";
    }

    //print_r ($errors);

    //what we need to return back to our form
      $returndata = array(
          'posted_form_data' => array(
              'user_nickname' => $user_login,
              'user_email' => $user_email,
              'user_birth_year' => $user_birth_year,
              'user_sex' => $user_sex,
              'user_grade' => $user_grade
          ),
          'errors' => $errors
      );

      session_start();
      $_SESSION['cf_returndata'] = $returndata;
  }
}

add_action('template_redirect', 'abation_custom_register_user');

function getUserOS() {
  $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $os_platform    =   "Unknown OS Platform";

  $os_array  =   array(
                        '/windows nt 10/i'     =>  'Windows 10',
                        '/windows nt 6.3/i'     =>  'Windows 8.1',
                        '/windows nt 6.2/i'     =>  'Windows 8',
                        '/windows nt 6.1/i'     =>  'Windows 7',
                        '/windows nt 6.0/i'     =>  'Windows Vista',
                        '/windows nt 5.2/i'     =>  'Windows Server 2003/XP x64',
                        '/windows nt 5.1/i'     =>  'Windows XP',
                        '/windows xp/i'         =>  'Windows XP',
                        '/windows nt 5.0/i'     =>  'Windows 2000',
                        '/windows me/i'         =>  'Windows ME',
                        '/win98/i'              =>  'Windows 98',
                        '/win95/i'              =>  'Windows 95',
                        '/win16/i'              =>  'Windows 3.11',
                        '/macintosh|mac os x/i' =>  'Mac OS X',
                        '/mac_powerpc/i'        =>  'Mac OS 9',
                        '/linux/i'              =>  'Linux',
                        '/ubuntu/i'             =>  'Ubuntu',
                        '/iphone/i'             =>  'iPhone',
                        '/ipod/i'               =>  'iPod',
                        '/ipad/i'               =>  'iPad',
                        '/android/i'            =>  'Android',
                        '/blackberry/i'         =>  'BlackBerry',
                        '/webos/i'              =>  'Mobile'
                    );

    foreach ($os_array as $regex => $value) { 
        if (preg_match($regex, $user_agent)) {
            $os_platform    =   $value;
        }
    }   

    return $os_platform;
}

function getUserBrowser() {
  $user_agent = $_SERVER['HTTP_USER_AGENT'];

    $browser        =   "Unknown Browser";

    $browser_array  =   array(
                            '/msie/i'       =>  'Internet Explorer',
                            '/firefox/i'    =>  'Firefox',
                            '/safari/i'     =>  'Safari',
                            '/chrome/i'     =>  'Chrome',
                            '/edge/i'       =>  'Edge',
                            '/opera/i'      =>  'Opera',
                            '/netscape/i'   =>  'Netscape',
                            '/maxthon/i'    =>  'Maxthon',
                            '/konqueror/i'  =>  'Konqueror',
                            '/mobile/i'     =>  'Handheld Browser'
                        );

    foreach ($browser_array as $regex => $value) { 
        if (preg_match($regex, $user_agent)) {
            $browser    =   $value;
        }
    }

    return $browser;
}

function getUserDevice() {
  $tablet_browser = 0;
  $mobile_browser = 0;

  if (preg_match('/(tablet|ipad|playbook)|(android(?!.*(mobi|opera mini)))/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
      $tablet_browser++;
  }

  if (preg_match('/(up.browser|up.link|mmp|symbian|smartphone|midp|wap|phone|android|iemobile)/i', strtolower($_SERVER['HTTP_USER_AGENT']))) {
      $mobile_browser++;
  }

  if ((strpos(strtolower($_SERVER['HTTP_ACCEPT']),'application/vnd.wap.xhtml+xml') > 0) or ((isset($_SERVER['HTTP_X_WAP_PROFILE']) or isset($_SERVER['HTTP_PROFILE'])))) {
      $mobile_browser++;
  }

  $mobile_ua = strtolower(substr($_SERVER['HTTP_USER_AGENT'], 0, 4));
  $mobile_agents = array(
      'w3c ','acs-','alav','alca','amoi','audi','avan','benq','bird','blac',
      'blaz','brew','cell','cldc','cmd-','dang','doco','eric','hipt','inno',
      'ipaq','java','jigs','kddi','keji','leno','lg-c','lg-d','lg-g','lge-',
      'maui','maxo','midp','mits','mmef','mobi','mot-','moto','mwbp','nec-',
      'newt','noki','palm','pana','pant','phil','play','port','prox',
      'qwap','sage','sams','sany','sch-','sec-','send','seri','sgh-','shar',
      'sie-','siem','smal','smar','sony','sph-','symb','t-mo','teli','tim-',
      'tosh','tsm-','upg1','upsi','vk-v','voda','wap-','wapa','wapi','wapp',
      'wapr','webc','winw','winw','xda ','xda-');
   
  if (in_array($mobile_ua,$mobile_agents)) {
      $mobile_browser++;
  }
   
  if (strpos(strtolower($_SERVER['HTTP_USER_AGENT']),'opera mini') > 0) {
      $mobile_browser++;
      //Check for tablets on opera mini alternative headers
      $stock_ua = strtolower(isset($_SERVER['HTTP_X_OPERAMINI_PHONE_UA'])?$_SERVER['HTTP_X_OPERAMINI_PHONE_UA']:(isset($_SERVER['HTTP_DEVICE_STOCK_UA'])?$_SERVER['HTTP_DEVICE_STOCK_UA']:''));
      if (preg_match('/(tablet|ipad|playbook)|(android(?!.*mobile))/i', $stock_ua)) {
        $tablet_browser++;
      }
  }
   
  if ($tablet_browser > 0) {
     // do something for tablet devices
     return 'tablet';
  }
  else if ($mobile_browser > 0) {
     // do something for mobile devices
     return 'mobile';
  }
  else {
     // do something for everything else
     return 'desktop';
  }   
}

/**
 * Custom Forgot Password.
 */

function abation_custom_forgot_password() {
  if(isset($_GET['do']) && $_GET['do'] == 'forgot-password') {

    $errors = array();
    $user_login = null;
    session_start();

    if(isset($_POST['user_email']) && wp_verify_nonce($_POST['abation_forgot_password_nonce'], 'abation-forgot-password-nonce')) {

      // this returns the user ID and other info from the user name
      $user_email = $_POST["user_email"];
      $user = get_user_by('email',$user_email);

      //require_once(ABSPATH  . '/wp-login.php');  
      if ($user) {
        if (abation_customized_retrieve_password($user->user_login)) {
          //OK
          /*wp_redirect(home_url()); 
          die;*/
          $_SESSION['success'] = "ok";
        }
        else {
          $errors[] = 'Email incorrecto';
          $_SESSION['errors'] = $errors;
        }
      }
      else {
        $errors[] = 'Email incorrecto';
        $_SESSION['errors'] = $errors;
      }
    }
    else {
      //Nonce does not match or maybe the user_login is not set
      $errors[] = 'Parece que hemos hecho algo mal :( Por favor, inténtalo de nuevo';
      $_SESSION['errors'] = $errors;
    }
  }
}

add_action('template_redirect', 'abation_custom_forgot_password');

/**
 * Custom Restart Password
 **/

function abation_custom_restart_password() {
  if(isset($_GET['do']) && $_GET['do'] == 'restart-password') {
    if(isset($_POST['user_password']) && !empty($_POST['user_password']) 
    && wp_verify_nonce($_POST['abation_restart_password_nonce'], 'abation-restart-password-nonce')) {
      if (preg_match('/^.{6,}$/', $_POST['user_password'])) {
        //OK!
        $loginFromUrl = wp_unslash($_POST["user_login_url_param"]);//Esto lo he setteado yo en el view
        $keyFromUrl = wp_unslash($_POST["user_key_url_param"]);//Esto lo he setteado yo en el view

        $user = check_password_reset_key($keyFromUrl, $loginFromUrl);

        $errors = new WP_Error();

        /**
         * Fires before the password reset procedure is validated.
         *
         * @since 3.5.0
         *
         * @param object           $errors WP Error object.
         * @param WP_User|WP_Error $user   WP_User object if the login and reset key match. WP_Error object otherwise.
         */
        do_action( 'validate_password_reset', $errors, $user );
        
        if ( ( !$errors->get_error_code() ) && isset( $_POST['user_password'] ) && !empty( $_POST['user_password'] ) ) { 
          reset_password($user, $_POST['user_password']);

          wp_setcookie($user->user_login, $_POST['user_password'], true);
          wp_set_current_user($user->ID, $user->user_login);  
          do_action('wp_login', $user->user_login);
     
          wp_redirect(home_url()); 
          exit;
        }
        else {
          $returndata = array(
                'errors' => 'Parece que ha habido algún error. Es posible que haya expirado el tiempo de vida del enlace que te hemos enviado por email'
            );

            session_start();
          $_SESSION['cf_returndata'] = $returndata;
        }
      }
      else {
        $returndata = array(
              'errors' => 'La contraseña debe tener al menos 6 caracteres'
          );

          session_start();
        $_SESSION['cf_returndata'] = $returndata;
      }
    }
    else {
      $returndata = array(
            'errors' => 'Parece que hemos hecho algo mal :( Por favor, inténtalo de nuevo'
        );

        session_start();
      $_SESSION['cf_returndata'] = $returndata;
    }
  }
}

add_action('template_redirect', 'abation_custom_restart_password');

/**
 * Método encargado de enviar el email de recuperación de contraseña al usuario 
 * a partir de su user_login (que puede ser el email o el username).
 * Además también genera los hash etc necesarios para la validación.
 **/

function abation_customized_retrieve_password($user_login) {
  global $wpdb, $wp_hasher;

  error_log("¡Dentroooooo!", 0);

    $user_login = sanitize_text_field($user_login);

    if ( empty( $user_login) ) {
        return false;
    } else if ( strpos( $user_login, '@' ) ) {
        $user_data = get_user_by( 'email', trim( $user_login ) );
        if ( empty( $user_data ) )
           return false;
    } else {
        $login = trim($user_login);
        $user_data = get_user_by('login', $login);
    }

    do_action('lostpassword_post');

    if ( !$user_data ) return false;

    // redefining user_login ensures we return the right case in the email
    $user_login = $user_data->user_login;
    $user_email = $user_data->user_email;

    do_action('retreive_password', $user_login);  // Misspelled and deprecated
    do_action('retrieve_password', $user_login);

    /**
   * Filter whether to allow a password to be reset.
   *
   * @since 2.7.0
   *
   * @param bool true           Whether to allow the password to be reset. Default true.
   * @param int  $user_data->ID The ID of the user attempting to reset a password.
   */
    $allow = apply_filters('allow_password_reset', true, $user_data->ID);

    if ( ! $allow )
        return false;
    else if ( is_wp_error($allow) )
        return false;

    // Generate something random for a password reset key.
    $key = wp_generate_password( 20, false );

    /**
   * Fires when a password reset key is generated.
   *
   * @since 2.5.0
   *
   * @param string $user_login The username for the user.
   * @param string $key        The generated password reset key.
   */
    do_action( 'retrieve_password_key', $user_login, $key );

    // Now insert the key, hashed, into the DB.
    if ( empty( $wp_hasher ) ) {
        require_once ABSPATH . 'wp-includes/class-phpass.php';
        $wp_hasher = new PasswordHash( 8, true );
    }
    $hashed = time() . ':' . $wp_hasher->HashPassword( $key );
    $wpdb->update( $wpdb->users, array( 'user_activation_key' => $hashed ), array( 'user_login' => $user_login ) );

    error_log("generated key: " . $key, 0);
    error_log("hashed generated key: " . $hashed, 0);

    $message = __('Someone requested that the password be reset for the following account:') . "\r\n\r\n";
    //$message .= network_home_url( '/' ) . "\r\n\r\n";
    $message .= sprintf(__('Username: %s'), $user_login) . "\r\n\r\n";
    $message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
    $message .= __('To reset your password, visit the following address:') . "\r\n\r\n";
    $message .= '<' . network_site_url("restart-password?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . ">\r\n";

    if ( is_multisite() )
        $blogname = $GLOBALS['current_site']->site_name;
    else
        $blogname = wp_specialchars_decode(get_option('blogname'), ENT_QUOTES);

    $title = sprintf( __('[%s] Password Reset'), $blogname );

    $title = apply_filters('retrieve_password_title', $title);
    $message = apply_filters('retrieve_password_message', $message, $key);

    if ( $message && !wp_mail($user_email, $title, $message) )
        wp_die( __('The e-mail could not be sent.') . "<br />\n" . __('Possible reason: your host may have disabled the mail() function...') );

    //echo '<p>Link for password reset has been emailed to you. Please check your email.</p>';
    return true;
}

/**
 * Método encargado de enviar el email a un usuario que acaba de registrarse 
 **/

function sendEmailToNewUser( $user_id, $role = false) {
  $user     =   new WP_User($user_id);
  $user_email   = $user->user_email;

  if(ae_get_option( 'user_confirm' )){
    $message    = ae_get_option('confirm_mail_template');
  } else {
    $message    = ae_get_option('register_mail_template');
  }
  $message   =  stripslashes($message);
  $message    = et_filter_authentication_placeholder ( $message, $user_id );
  $subject    = sprintf(__("Congratulations! You have successfully registered to %s.",ET_DOMAIN),get_option('blogname'));

  $headers  = 'MIME-Version: 1.0' . "\r\n";
  $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
  $headers .= "From: ".get_option('blogname')." < ".ae_get_option('send_mail_from', 'academia@abation.com')."> \r\n";

  wp_mail($user_email, $subject , $message, $headers) ;
}

/**
 * Custom Anonymous Question.
 */

function abation_custom_new_anonymous_question() {
  error_log("¡Dentro de la función anonymous question! 1", 0);
  if(isset($_GET['do']) && $_GET['do'] == 'anonymous-question') {

    error_log("¡Dentro de la función anonymous question! 2", 0);

    $errors = array();
    $user_email = 'anonimo@abation.com';
    session_start();

    if(wp_verify_nonce($_POST['abation_anonymous_question_nonce'], 'abation-anonymous-question-nonce')) {

      error_log("¡Dentro de la función anonymous question! 3", 0);

      // this returns the user ID and other info from the user name
      $user = get_user_by('email',$user_email);

      //require_once(ABSPATH  . '/wp-login.php');  
      if ($user) {

        error_log("¡Dentro de la función anonymous question! 4", 0);

        /*$user_ip = $_SERVER['REMOTE_ADDR'];//Should save this into db to get some data from user

        error_log($_POST['question_category'], 0);

        // Create post object
        $anonymous_question = array(
          'post_title'    => $_POST['question_title'],
          'post_content'  => $_POST['question_content'],
          'post_status'   => 'publish',
          'post_type' => 'question',
          'post_author'   => $user->ID
        );

        // Insert the post into the database
        wp_insert_post( $anonymous_question );
        wp_redirect(home_url()); 
        exit;*/

        $cats = array(
          /*'qa_tag'      => $_POST['tags'],*/
          'question_category' => $_POST['question_category']
        );

        wp_set_current_user($user->ID, $user->user_login);  

        $result = QA_Questions::insert_question($_POST['question_title'],$_POST['question_content'],$cats,"publish");
        do_action( 'qa_insert_question', $result );

        wp_logout();//clear and destroy current user info (session, etc)

        if(!is_wp_error( $result )){
          wp_redirect( get_permalink( $result ) );
          //wp_logout_url( get_permalink($result) );
          exit;
        }
        else {
          $errors[] = $result->get_error_message();
        }
      }
      else {
        error_log("Error, parece que el $user no existe", 0);
      }
    }

    //Nonce does not match or maybe the user_login is not set
    $_SESSION['errors'] = $errors;
  }
}

add_action('template_redirect', 'abation_custom_new_anonymous_question');