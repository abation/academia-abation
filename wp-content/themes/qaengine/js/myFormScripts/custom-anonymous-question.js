
var globals = {
  formID: "#mainForm"
};

$(document).ready(function() {
  $('#modal-login').on('click', function() {
    ga('send', 'event', 'button', 'click', 'anonymous question modal login');
  });

  $('#modal-register').on('click', function() {
    ga('send', 'event', 'button', 'click', 'anonymous question modal register');
  });

  $('#modal-close').on('click', function() {
    ga('send', 'event', 'button', 'click', 'anonymous question modal close');
  });

  RegisterOnFormSubmitHandlers();
});

function RegisterOnFormSubmitHandlers() {
  $(globals.formID).submit(function(e) {
    //e.preventDefault();
    ga('send', 'event', 'button', 'click', 'custom anonymous question form submit');

    var valid = true;
    
    var show_error_message = function (element) {
      $("#" + element.attr("id") + "_error").show();
      valid = false;
    }

    var question_title = $("#question_title");
    var question_category = $("#question_category");

    if(!chk_is_empty(question_title.val()))
      show_error_message(question_title);

    if (!chk_is_select_valid(question_category.val()))
      show_error_message(question_category);

    //If all the fields are valid, then register user
    if (valid)
      //sendDataToServer(email.val());
      return true;
    else
      return false;
  })
}

//This function give us the content of the query string param we send by param (name)
function getParameterByName( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );

  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Make the validation of the inputs
function chk_is_empty(input_value) {
  if(input_value == null || input_value == "") {
    return false;
  }

  return true;
}

function chk_is_select_valid(selected_value) {
  //0 = a la default
  if (selected_value == 0 || selected_value == "" || selected_value == null) {
    return false;
  }

  return true;
}

/*********************Focus events; remove error messages**********************/
function onFocusQuestionTitle() {
  $("#question_title_error").hide();
  $("#question_title_error").text("Debes escribir un título para tu pregunta");
}

function onFocusQuestionCategory() {
  $("#question_category_error").hide();
  $("#question_category_error").text("Debes elegir una categoría para tu pregunta");
}
