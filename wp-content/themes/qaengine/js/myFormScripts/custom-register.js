
var globals = {
  formID: "#mainForm"
};

$(document).ready(function() {
  //focus on the email input
  $("#user_email").focus();

  //Get param "dinamic_text" of query string
  var punch = getParameterByName("punch");
  if(punch != "" && punch != null && punch != undefined)
  {
    var newString = punch.replace(/_/g, " ");
    $("#punch").text(newString);
  }

  //Get param "ad_tracking" of query string
  var ad_tracking = getParameterByName("ad_tracking");
  if(ad_tracking != "" && ad_tracking != null && ad_tracking != undefined)
  {
    $("#ad_tracking").val(ad_tracking);
  }

  RegisterOnFormSubmitHandlers();
});

function RegisterOnFormSubmitHandlers(){

  $(globals.formID).submit(function(e){
    //e.preventDefault();
    ga('send', 'event', 'button', 'click', 'custom register form submit');

    var valid = true;
    
    var show_error_message = function (element) {
      $("#" + element.attr("id") + "_error").show();
      valid = false;
    }

    var email = $("#user_email");
    var nickname = $("#user_nickname");
    var password = $("#user_password");
    var sex = $("input[name=user_sex]:checked");
    var birth_year = $("#user_birth_year");
    var user_grade = $("#user_grade");

    if(!chk_is_valid_email(email.val()))
      show_error_message(email);

    if (!chk_is_valid_nickname(nickname.val()))
      show_error_message(nickname);
    
    if (!chk_is_valid_password(password.val()))
      show_error_message(password);

    if (!chk_is_empty(sex.val())) {
      $("#user_sex_error").show();
      valid = false;
    }

    if (!chk_is_select_valid(birth_year.val()))
      show_error_message(birth_year);

    if (!chk_is_select_valid(user_grade.val()))
      show_error_message(user_grade);

    //If all the fields are valid, then register user
    if (valid)
      return true;
    else
      return false;
  })
}

//This function give us the content of the query string param we send by param (name)
function getParameterByName( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );

  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Make the validation of the inputs
function chk_is_empty(input_value) {
  if(input_value == null || input_value == "") {
    return false;
  }

  return true;
}

//Validate password (more than 6 characters)
function chk_is_valid_password(password) {
  var filter = /^.{6,}$/

  if(password != "" && password != null && password != undefined && filter.test(password)) {
    return true;
  }
}

function chk_is_valid_nickname(nickname) {
  var filter = /^.{4,}$/

  if(nickname != "" && nickname != null && nickname != undefined && filter.test(nickname)) {
    return true;
  }
}

//Make the validation of the email input
function chk_is_valid_email(email_address)
{
  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

  if (filter.test(email_address)){
    testresults=true
  }
  else{
    testresults=false
  }

  return (testresults)
}


/*function atLeastOneRadio() {
    return ($('input[type=radio]:checked').size() > 0);
}*/

function chk_is_select_valid(selected_value)
{
  //0 = a la default
  if (selected_value == 0) {
    return false;
  }

  return true;
}

/********************* Focus events; remove error messages **********************/
function onFocusEmail() {
  $("#user_email_error").hide();
  $("#user_email_error").text("Email inválido");
}

function onFocusNickname() {
  $("#user_nickname_error").hide();
  $("#user_nickname_error").text("El nombre de usuario debe tener al menos 4 caracteres");
}

function onFocusPassword() {
  $("#user_password_error").hide();
  $("#user_password_error").text("La contraseña debe tener al menos 6 caracteres");
}

function onFocusSex() {
  $("#user_sex_error").hide();
  $("#user_sex_error").text("Debes elegir una opción");
}

function onFocusBirthYear() {
  $("#user_birth_year_error").hide();
  $("#user_birth_year_error").text("Debes elegir una opción");
}

function onFocusGrade() {
  $("#user_grade_error").hide();
  $("#user_grade_error").text("Debes elegir una opción");
}
