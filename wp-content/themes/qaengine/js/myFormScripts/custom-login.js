
var globals = {
  formID: "#mainForm"
};

$(document).ready(function() {
  //focus on the email input
  $("#user_email").focus();

  //Get param "dinamic_text" of query string
  var punch = getParameterByName("punch");
  if(punch != "" && punch != null && punch != undefined) {
    var newString = punch.replace(/_/g, " ");
    $("#punch").text(newString);
  }

  //Get param "ad_tracking" of query string
  var ad_tracking = getParameterByName("ad_tracking");
  if(ad_tracking != "" && ad_tracking != null && ad_tracking != undefined) {
    $("#ad_tracking").val(ad_tracking);
  }

  var url_info_param = getParameterByName("type");
  if (url_info_param != "" && url_info_param != null && url_info_param != undefined) {
    switch (url_info_param) {
      case "contact":
        $("#customized-info-message").text("Es necesario estar logueado para enviar un mensaje privado");
        $("#customized-info-message").show();
        break;

      case "question":
        $("#customized-info-message").text("Es necesario estar logueado para hacer una pregunta");
        $("#customized-info-message").show();
        break;

      case "answer":
        $("#customized-info-message").text("Es necesario estar logueado para responder a una pregunta");
        $("#customized-info-message").show();
        break;

      default:
        break;
    }
  }

  userFromLP(); 

  RegisterOnFormSubmitHandlers();
});

function userFromLP() {
  //var email = getParameterByName("email");
  var emailName = getParameterByName("emailName");
  var emailDomain = getParameterByName("emailDomain");
  var password = getParameterByName("password");

  if(emailName != "" && emailName != null && emailName != undefined &&
    emailDomain != "" && emailDomain != null && emailDomain != undefined &&
    password != "" && password != null && password != undefined) {

    var email = createEmailFromParts(emailName, emailDomain);

    //Set values in form inputs
    $("#user_email").val(email);
    $("#user_password").val(password);

    //Hide links
    $("#forgot_password_link").hide();
    $("#register_mssg_bottom").hide();
    $("#login_qa").hide();

    //Select remember checkbox
    $("#rememberme").prop("checked",true);

    //Set values in thanks_info div
    $("#email_response").text(email);
    $("#password_response").text(password);

    //Finally, show div
    $("#thanks_info").show();
  }
}

function createEmailFromParts(emailName, emailDomain) {
  return emailName + '@' + emailDomain;
}

function RegisterOnFormSubmitHandlers(){

  $(globals.formID).submit(function(e){

    ga('send', 'event', 'button', 'click', 'custom login form submit');

    //e.preventDefault();
    var valid = true;
    
    var show_error_message = function (element) {
      $("#" + element.attr("id") + "_error").show();
      valid = false;
    }

    var email = $("#user_email");
    var password = $("#user_password");

    if(!chk_is_valid_email(email.val()))
      show_error_message(email);
    
    if (!chk_is_empty(password.val()))
      show_error_message(password);

    //If all the fields are valid, then register user
    if (valid)
      return true;
    else
      return false;
  })
}

//This function give us the content of the query string param we send by param (name)
function getParameterByName( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );

  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

//Make the validation of the inputs
function chk_is_empty(input) {
  if(input == null || input == "") {
    return false;
  }

  return true;
}

//Make the validation of the email input
function chk_is_valid_email(email_address) {
  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

  if (filter.test(email_address)){
    testresults=true
  }
  else{
    testresults=false
  }

  return (testresults)
}

/*********************Focus events; remove error messages**********************/
function onFocusEmail() {
  $("#user_email_error").hide();
  $("#user_email_error").text("Email inválido");
}

function onFocusPassword() {
  $("#user_password_error").hide();
  $("#user_password_error").text("Contraseña inválida");
}
