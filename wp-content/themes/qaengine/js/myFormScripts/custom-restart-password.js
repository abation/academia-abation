
var globals = {
  formID: "#mainForm"
};

$(document).ready(function() {
  RegisterOnFormSubmitHandlers();
});

function RegisterOnFormSubmitHandlers() {
  $(globals.formID).submit(function(e) {
    //e.preventDefault();
    ga('send', 'event', 'button', 'click', 'custom restart password form submit');

    var valid = true;
    
    var show_error_message = function (element) {
      $("#" + element.attr("id") + "_error").show();
      valid = false;
    }

    var new_password = $("#user_password");

    if(!chk_is_valid_new_password(new_password.val()))
      show_error_message(new_password);

    //If all the fields are valid, then register user
    if (valid)
      //sendDataToServer(email.val());
      return true;
    else
      return false;
  })
}

//This function give us the content of the query string param we send by param (name)
function getParameterByName( name ) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );

  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}

function chk_is_valid_new_password(new_password) {
  var filter = /^.{6,}$/
  
  if(new_password != "" && new_password != null && new_password != undefined && filter.test(new_password)) {
    return true;
  }

  return false;
}


/*********************Focus events; remove error messages**********************/
function onFocusPassword() {
  $("#user_password_error").hide();
  $("#user_password_error").text("La contraseña debe tener al menos 6 caracteres");
}
