
var globals = {
  formID: "#mainForm"
};

$(document).ready(function() {
  RegisterOnFormSubmitHandlers();
});

function RegisterOnFormSubmitHandlers() {

  $(globals.formID).submit(function(e){
    //e.preventDefault();
    ga('send', 'event', 'button', 'click', 'custom forgot password form submit');

    var valid = true;
    
    var show_error_message = function (element) {
      $("#" + element.attr("id") + "_error").show();
      valid = false;
    }

    var user_email = $("#user_email");

    if(!chk_is_valid_email(user_email.val()))
      show_error_message(user_email);

    //If all the fields are valid, then register user
    if (valid)
      //sendDataToServer(email.val());
      return true;
    else
      return false;
  })
}

//Make the validation of the email input
function chk_is_valid_email(email_address) {
  var filter=/^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i

  if (filter.test(email_address)){
    testresults=true
  }
  else{
    testresults=false
  }

  return (testresults)
}


/*********************Focus events; remove error messages**********************/
function onFocusEmail() {
  $("#user_email_error").hide();
  $("#user_email_error").text("Email inválido");
}
