$(document).ready(function() {
  var email = getParameterByName("email");
  var login = getParameterByName("login");
  var password = getParameterByName("password");

  if(login != "" && login != null && login != undefined &&
    password != "" && password != null && password != undefined &&
    email != "" && email != null && email != undefined ) {

    //Set values in form inputs
    $("#user_login").val(login);
    $("#user_pass").val(password);

    //Hide bottom navigation buttons
    $("#nav").hide();
    $("#backtoblog").hide();

    //Select remember checkbox
    $("#rememberme").prop("checked",true);

    //Set values in thanks_info div
    $("#email_response").text(email);
    $("#password_response").text(password);
    $("#username_response").text(login);
    $("#thanks_info").show();//Show div
  }
});

//This function give us the content of the query string param we send by param (name)
function getParameterByName(name) {
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );

  if( results == null )
    return "";
  else
    return decodeURIComponent(results[1].replace(/\+/g, " "));
}
