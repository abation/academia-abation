server {
       listen 80;
       server_name www.academia.abation.es academia.abation.es;
       root /var/www/ftp/academia.abation.es;

       if ($http_host != "academia.abation.es") {
                 rewrite ^ http://academia.abation.es$request_uri permanent;
       }

       index index.php index.html index.htm default.html default.htm;

       location = /favicon.ico {
                log_not_found off;
                access_log off;
       }

       location = /robots.txt {
                allow all;
                log_not_found off;
                access_log off;
       }

       # Deny all attempts to access hidden files such as .htaccess, .htpasswd, .DS_Store (Mac).
       location ~ /\. {
                deny all;
                access_log off;
                log_not_found off;
       }

	location / {
                try_files $uri $uri/ /index.php?q=$uri&$args;
       }
      

       location ~ \.php$ {
                try_files $uri =404;
                fastcgi_split_path_info ^(.+\.php)(/.+)$;
                fastcgi_pass unix:/var/run/php5-fpm.sock; # use this if PHP-FPM is running on Unix socket /var/run/php5-fpm.sock (Ubuntu 12.10 default)
                #fastcgi_pass 127.0.0.1:9000; # use this if PHP-FPM is running on TCP port 9000 (Debian Squeeze default)
                include /etc/nginx/fastcgi_params;
                fastcgi_index index.php;
       }
	
	location ~* \.html$ {
 	 	expires -1;
	}

	location ~* \.(css|js|gif|jpe?g|png)$ {
	  	expires 30d;
  		add_header Pragma public;
  		add_header Cache-Control "public, must-revalidate, proxy-revalidate";
	}
	
	location ~* \.(php|html)$ {
		access_log on;
		log_not_found on;
		add_header Pragma public;
		add_header Cache-Control "public";
		expires 14d;
	}	

}
